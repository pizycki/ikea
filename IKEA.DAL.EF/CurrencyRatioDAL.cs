﻿using IKEA.Common.Contracts.DAL;
using IKEA.Common.Contracts.POCO;
using IKEA.Common.Models.DAL.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Data.Entity.Validation;
using System.Diagnostics;
using IKEA.Common.Utils;

namespace IKEA.DAL.EF
{
    public class CurrencyRatioDal : ICurrencyRatioRepository
    {
        readonly IMapperWithObjectCreation<ICurrencyRatio, CurrencyRatio> _mapper;

        public CurrencyRatioDal(IMapperWithObjectCreation<ICurrencyRatio, CurrencyRatio> mapper)
        {
            _mapper = mapper;
        }

        private CurrencyRatio MapCurrencyRatio(ICurrencyRatio ratio)
        {
            return _mapper.CreateMappedObject(ratio);
        }

        public void Add(params ICurrencyRatio[] items)
        {
            var mappedRatios = items.Select(MapCurrencyRatio).ToList();

            // Jeżeli jakiś obiekt ma puste pole z czasem stworzenia, nadaj aktualny czas
            mappedRatios.ForEach(item => item.CreationTime = item.CreationTime ?? DateTime.Now);

            using (var ctx = new CurrenciesContext())
            {
                foreach (var ratioEntity in mappedRatios)
                {
                    // Attach existing currency entities

                    Attach(ratioEntity.Base, ctx);
                    Attach(ratioEntity.Target, ctx);
                    ctx.CurrencyRatios.Add(ratioEntity);
                }

                try
                {
                    ctx.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                            Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                    throw;
                }
            }
        }

        private void Attach(Currency currency, CurrenciesContext ctx)
        {
            if (ctx.Currencies.Local.All(x => x != currency))
                ctx.Currencies.Attach(currency);
        }

        public IEnumerable<ICurrencyRatio> GetAll()
        {
            using (var ctx = new CurrenciesContext())
            {
                return ctx.CurrencyRatios
                    .Include(c => c.Base)
                    .Include(c => c.Target)
                    .ToList();
            }
        }

        public ICurrencyRatio GetSingle(string ratioBaseAbbreviation, string ratioTargetAbbreviation)
        {
            using (var ctx = new CurrenciesContext())
            {
                return ctx.CurrencyRatios
                    .Include(c => c.Base)
                    .Include(c => c.Target)
                    .Where(c => c.Base.Abbreviation == ratioBaseAbbreviation && c.Target.Abbreviation == ratioTargetAbbreviation)
                    .OrderByDescending(r => r.CreationTime)
                    .FirstOrDefault();
            }
        }

        public ICurrencyRatio GetSingle(int id)
        {
            using (var ctx = new CurrenciesContext())
            {
                return ctx.CurrencyRatios
                    .Include(c => c.Base)
                    .Include(c => c.Target)
                    .SingleOrDefault(x => x.CurrencyRatioId == id);
            }
        }
    }
}
