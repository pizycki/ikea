﻿using IKEA.Common.Attributes.Caching;
using IKEA.Common.Contracts.DAL;
using IKEA.Common.Contracts.POCO;
using IKEA.Common.Models.DAL.EF;
using System.Collections.Generic;
using System.Linq;
using IKEA.Common.Utils;

namespace IKEA.DAL.EF
{
    public class CurrencyDal : ICurrencyRepository
    {
        readonly IMapperWithObjectCreation<ICurrency, Currency> _mapper;

        public CurrencyDal(IMapperWithObjectCreation<ICurrency, Currency> mapper)
        {
            _mapper = mapper;
        }

        [Cached]
        public IEnumerable<ICurrency> GetAll()
        {
            using (var ctx = new CurrenciesContext())
            {
                return ctx.Currencies.ToList().Select(x => (ICurrency)x);
            }
        }


        [Cached]
        public ICurrency GetSingle(int id)
        {
            using (var ctx = new CurrenciesContext())
            {
                return ctx.Currencies.SingleOrDefault(x => x.CurrencyId == id);
            }
        }


        [Cached]
        public IEnumerable<ICurrency> GetManyByAbbreviation(IEnumerable<string> abbreviations)
        {
            using (var ctx = new CurrenciesContext())
            {
                return ctx.Currencies.Where(x => abbreviations.Contains(x.Abbreviation)).ToList();
            }
        }

        [Cached]
        public virtual ICurrency GetByAbbreviation(string abbreviation)
        {
            using (var ctx = new CurrenciesContext())
            {
                return ctx.Currencies.SingleOrDefault(x => x.Abbreviation == abbreviation);
            }
        }

        private Currency MapCurrency(ICurrency currency)
        {
            return _mapper.CreateMappedObject(currency);
        }

        public void Add(params ICurrency[] items)
        {
            var mappedCurrencies = items.Select(MapCurrency);

            using (var ctx = new CurrenciesContext())
            {
                ctx.Currencies.AddRange(mappedCurrencies);
                ctx.SaveChanges();
            }
        }
    }
}
