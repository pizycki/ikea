﻿using IKEA.Common.Models.DAL.EF;
using System.Data.Entity;

namespace IKEA.DAL.EF
{
    public class CurrenciesContext : DbContext
    {
        const string ContextName = "IkeaDB";

        public CurrenciesContext()
            : base(ContextName)
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Currency> Currencies { get; set; }
        public DbSet<CurrencyRatio> CurrencyRatios { get; set; }
    }
}
