namespace IKEA.DAL.EF.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<CurrenciesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(CurrenciesContext context)
        {
            // Add initial data

            //var currencies = new List<Currency>{
            //    new Currency{Name="Euro", Abbreviation="EUR"},
            //    new Currency{Name="American dollar",Abbreviation="USD"}
            //};

            //currencies.ForEach(item => ctx.Currencies.Add(item));

            //ctx.SaveChanges();
        }
    }
}
