﻿using IKEA.Application.Validators;
using IKEA.Common.Contracts.Application.Validators;
using IKEA.Common.Contracts.POCO;
using Moq;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace IKEA.BLL.Tests
{
    [TestClass]
    public class CurrencyValidatorTests
    {
        ICurrencyValidatorFactory _currencyValidatorFactory;
        ICurrencyRatioValidatorFactory _currencyRatioValidatorFactory;

        public ICurrency GenerateMockCurrency(String name, String abbrv, String country)
        {
            var mockedCurrency = new Mock<ICurrency>();

            mockedCurrency.Setup(x => x.Name).Returns(name);
            mockedCurrency.Setup(x => x.Abbreviation).Returns(abbrv);
            mockedCurrency.Setup(x => x.Country).Returns(country);

            return mockedCurrency.Object;
        }

        public ICurrencyRatio GenerateMockCurrencyRatio(ICurrency baseCurrency, ICurrency targetCurrency, float currencyValue, DateTime time)
        {
            Mock<ICurrencyRatio> mockedCurrencyRatio = new Mock<ICurrencyRatio>();

            mockedCurrencyRatio.Setup(x => x.Base).Returns(baseCurrency);
            mockedCurrencyRatio.Setup(x => x.Target).Returns(targetCurrency);
            mockedCurrencyRatio.Setup(x => x.Value).Returns(currencyValue);

            return mockedCurrencyRatio.Object;
        }

        [TestInitialize]
        public void Initialize()
        {
            _currencyValidatorFactory = new CurrencyValidatorFactory();
            _currencyRatioValidatorFactory = new CurrencyRatioValidatorFactory(_currencyValidatorFactory);
        }

        [TestMethod]
        public void Currency_Abbrv_Should_Have_Only_Big_Letters()
        {
            //Arrange
            ICurrencyValidator currencyValidator = _currencyValidatorFactory.Create();

            string error1 = string.Empty;
            string error2 = string.Empty;

            var currencySmallLetters = GenerateMockCurrency("Zloty", "Pl3", null);

            var currencyBigLetters = GenerateMockCurrency("Zloty", "PLN", null);

            //Act
            var small = currencyValidator.Validate(currencySmallLetters, out error1);
            var big = currencyValidator.Validate(currencyBigLetters, out error2);

            //Assert
            Assert.AreEqual(false, small);
            Assert.AreEqual(true, big);
        }

        [TestMethod]
        public void Abbrv_Should_Be_3_Letters_Long()
        {
            //Arrange
            ICurrencyValidator currencyValidator = _currencyValidatorFactory.Create();

            string error1 = string.Empty;
            string error2 = string.Empty;

            var currencyAbbrv4Letters = GenerateMockCurrency("Zloty", "PLNY", null);

            var currencyAbbrv3Letters = GenerateMockCurrency("Zloty", "PLN", null);

            //Act

            var letters4 = currencyValidator.Validate(currencyAbbrv4Letters, out error1);
            var letters3 = currencyValidator.Validate(currencyAbbrv3Letters, out error2);

            //Assert

            Assert.AreEqual(false, letters4);
            Assert.AreEqual(true, letters3);
        }

        [TestMethod]
        public void Value_Should_Be_Positive_Numer()
        {
            //Arrange
            ICurrencyRatioValidator currencyRatioValidator = _currencyRatioValidatorFactory.Create();

            var plnCurrency = GenerateMockCurrency("Zloty", "PLN", "Poland");
            var usdCurrency = GenerateMockCurrency("Dolar", "USD", "USA");

            string error1 = string.Empty;
            string error2 = string.Empty;

            var plnUsdRatio = GenerateMockCurrencyRatio(plnCurrency, usdCurrency, 3, DateTime.Now);
            var usdPlnRatio = GenerateMockCurrencyRatio(usdCurrency, plnCurrency, -3, DateTime.Now);

            //Act
            var shouldBeTrue = currencyRatioValidator.Validate(plnUsdRatio, out error1);
            var shouldBeFalse = currencyRatioValidator.Validate(usdPlnRatio, out error2);

            //Assert
            Assert.AreEqual(true, shouldBeTrue);
            Assert.AreEqual(false, shouldBeFalse);
        }

        [TestMethod]
        public void Ratio_Validator_Should_Correctly_Check_Currency()
        {
            //Arrange
            ICurrencyRatioValidator currencyRatioValidator = _currencyRatioValidatorFactory.Create();

            var plnCurrency = GenerateMockCurrency("Zloty", "pln", "Poland");
            var usdCurrency = GenerateMockCurrency("Dolar", "USD", "USA");
            var chfCurrency = GenerateMockCurrency("Frank", "CHF", "Swizerland");

            string error1 = string.Empty;
            string error2 = string.Empty;

            var plnUsdRatio = GenerateMockCurrencyRatio(plnCurrency, usdCurrency, 3, DateTime.Now);
            var usdChfRatio = GenerateMockCurrencyRatio(usdCurrency, chfCurrency, 4, DateTime.Now);

            //Act
            var shouldBeFalse = currencyRatioValidator.Validate(plnUsdRatio, out error1);
            var shouldBeTrue = currencyRatioValidator.Validate(usdChfRatio, out error2);

            //Assert
            Assert.AreEqual(false, shouldBeFalse);
            Assert.AreEqual(true, shouldBeTrue);
        }
    }
}
