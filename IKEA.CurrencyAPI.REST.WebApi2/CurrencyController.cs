﻿using IKEA.Common.Contracts.Application;
using IKEA.Common.Contracts.POCO;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace IKEA.CurrencyAPI.REST.WebApi2
{
    public class CurrencyController : ApiController
    {
        private readonly ICurrencyServer _currencyRateSrv;

        public CurrencyController() { }

        public CurrencyController(ICurrencyServer currencyRateSrv)
        {
            _currencyRateSrv = currencyRateSrv;
        }

        [HttpGet]
        public IEnumerable<ICurrency> List()
        {
            return _currencyRateSrv.GetCurrencies();
        }

        [HttpGet]
        public IEnumerable<string> Abbreviation()
        {
            return _currencyRateSrv.GetCurrencyAbbreviations();
        }

        [HttpGet]
        public IDictionary<string, ICurrency> Dictionary()
        {
            return _currencyRateSrv.GetCurrencyDictionary();
        }

        [HttpGet]
        public ICurrencyRatio Ratio(string baseAbrv, string targetAbrv)
        {
            var currencies = _currencyRateSrv.GetCurrencies();

            var baseCurr = currencies.Single(x => x.Abbreviation == baseAbrv.ToUpper());

            var targetCurr = currencies.Single(x => x.Abbreviation == targetAbrv.ToUpper());

            var result = _currencyRateSrv.GetCurrencyRatio(baseCurr, targetCurr);

            return result;
        }
    }
}
