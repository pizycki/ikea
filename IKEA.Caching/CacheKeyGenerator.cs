﻿using System.Text;
using IKEA.Common.Contracts.Application.Cache;

namespace IKEA.Caching
{
    public class CacheKeyGenerator : IMethodCacheKeyGenerator
    {
        public string GenerateCacheKeyForMethod(string methodName, params string[] parameters)
        {
            var sb = new StringBuilder();

            sb.Append(methodName);
            sb.Append(':');
            foreach (var param in parameters)
            {
                sb.Append(param);
                sb.Append(',');
            }

            return sb.ToString();
        }
    }
}