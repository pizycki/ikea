﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using IKEA.Common.Contracts.Application.Cache;

namespace IKEA.Caching
{
    public class Cache : ICache
    {
        private readonly ObjectCache _cache;

        public Cache()
        {
            _cache = MemoryCache.Default;
        }

        public bool Has(string key)
        {
            return _cache.Any(p => p.Key == key);
        }

        protected virtual bool Has(object item)
        {
            return _cache.Any(p => ReferenceEquals(p.Value, item));
        }

        public virtual object Retrieve(string key)
        {
            return _cache[key];
        }

        public virtual void Add(object item, string key)
        {
            if (string.IsNullOrEmpty(key) || Has(key) || Has(item))
                return;

            lock (_cache)
            {
                _cache.Add(key, item, GetNewItemPolicy);
            }
        }

        private static CacheItemPolicy GetNewItemPolicy
        {
            get
            {
                return new CacheItemPolicy { AbsoluteExpiration = Constants.DefaultItemExpirationTimeOffset };
            }
        }

        private IDictionary<string, object> Items
        {
            get { return _cache.ToDictionary(x => x.Key, x => x.Value); }
        }

        public override string ToString()
        {
            return string.Format("Cached items: {0}", Items.Count);
        }

        static class Constants
        {
            public static readonly DateTimeOffset DefaultItemExpirationTimeOffset = DateTimeOffset.Now.AddSeconds(120); // 2 min
        }

    }
}
