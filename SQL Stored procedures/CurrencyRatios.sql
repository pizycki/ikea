
--sp_GetCurrencyRatioByAbbreviations
CREATE PROCEDURE [dbo].[sp_GetCurrencyRatioByAbbreviations] 
	@bASeAbbr nvarchar(3)
	,@targetAbbr nvarchar(3)
AS 
BEGIN
	SELECT TOP 1
	[cr].[CurrencyRatioId] AS [CurrencyRatioId], 
	[cr].[CurrencyRatioValue] AS [CurrencyRatioValue], 
	[cr].[CurrencyRatioCreationTime] AS [CurrencyRatioCreationTime], 
	[cr].[CurrencyRatioBASeId] AS [CurrencyRatioBASeId], 
	[cr].[CurrencyRatioTargetId] AS [CurrencyRatioTargetId], 
	[bASeCurr].[CurrencyId] AS [BASeCurrencyId], 
	[bASeCurr].[CurrencyName] AS [BASeCurrencyName], 
	[bASeCurr].[CurrencyAbbreviation] AS [BASeCurrencyAbbreviation], 
	[bASeCurr].[CurrencyCountry] AS [BASeCurrencyCountry], 
	[trgtCurr].[CurrencyId] AS [TargetCurrencyId], 
	[trgtCurr].[CurrencyName] AS [TargetCurrencyName], 
	[trgtCurr].[CurrencyAbbreviation] AS [TargetCurrencyAbbreviation], 
	[trgtCurr].[CurrencyCountry] AS [TargetCurrencyCountry]
	FROM   [dbo].[CurrencyRatios] AS [cr]
	LEFT JOIN [dbo].[Currencies] AS [bASeCurr] ON [cr].[CurrencyRatioBASeId] = [bASeCurr].[CurrencyId]
	LEFT JOIN [dbo].[Currencies] AS [trgtCurr] ON [cr].[CurrencyRatioTargetId] = [trgtCurr].[CurrencyId]
	WHERE [bASeCurr].[CurrencyAbbreviation] = @bASeAbbr AND [trgtCurr].[CurrencyAbbreviation] = @targetAbbr
	ORDER BY CurrencyRatioCreationTime DESC
END

-- sp_AddCurrencyRatio
CREATE PROCEDURE [dbo].[sp_AddCurrencyRatio]
	@time datetime2(7) = NULL
	,@bASeCurrId int
	,@trgtCurrId int
	,@value real
AS 
BEGIN 
	IF (@time IS NULL) SET @time = GETDATE()	

	INSERT [dbo].[CurrencyRatios]
	(
		CurrencyRatioValue
		,CurrencyRatioCreationTime
		,CurrencyRatioBASeId
		,CurrencyRatioTargetId
	)
	VALUES (
		@value
		,@time
		,@bASeCurrId
		,@trgtCurrId
	)
	SELECT [CurrencyRatioId]
	FROM [dbo].[CurrencyRatios]
	WHERE @@ROWCOUNT > 0 AND [CurrencyRatioId] = scope_identity()
END