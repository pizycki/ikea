-- TODO Comment

-- sp_AddCurrency
create procedure [dbo].[sp_AddCurrency]
	@Name nvarchar(100)
	,@Abbreviation nvarchar(3)
	,@Country nvarchar(50)
	
as

begin 
	insert into Currencies
	(
		CurrencyName
		,CurrencyAbbreviation
		,CurrencyCountry
	)
	values
	(
		@Name
		,@Abbreviation
		,@Country
	)
end

-- sp_GetAllCurrencies
create procedure [dbo].[sp_GetAllCurrencies] as 
begin 
	select * from Currencies
end

-- sp_GetCurrencyByAbbreviation
create procedure [dbo].[sp_GetCurrencyByAbbreviation] @Abbr nvarchar(3) as 
begin
	select * from [dbo].[Currencies] where CurrencyAbbreviation = @Abbr
end

-- sp_GetCurrencyById
create procedure [dbo].[sp_GetCurrencyById] @id int as  
begin 
	select * from Currencies where Currencies.CurrencyId = @id
end