﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Castle.DynamicProxy;
using IKEA.Common.Attributes.Caching;
using IKEA.Common.Contracts.Application.Cache;
using IKEA.Common.Contracts.POCO;
using NLog;

namespace IKEA.AOP
{
    public class MethodCacheInterceptor : IInterceptor
    {
        private readonly ICache _cache;
        private readonly IMethodCacheKeyGenerator _cacheKeyGenerator;
        private readonly Logger _logger;

        public MethodCacheInterceptor(ICache cache, IMethodCacheKeyGenerator cacheKeyGenerator)
        {
            _cache = cache;
            _cacheKeyGenerator = cacheKeyGenerator;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public void Intercept(IInvocation invocation)
        {
            if (IsMethodCachable(invocation.MethodInvocationTarget))
                ProceedUsingCache(invocation);
            else
                invocation.Proceed();
        }

        private void ProceedUsingCache(IInvocation invocation)
        {
            var cacheKey = GenerateCacheKey(invocation);

            _logger.Debug("CacheKey: {0}", cacheKey);

            // Check is the seeked item kept in cache. If so, return it by passing inner body method.
            if (_cache.Has(cacheKey))
            {
                invocation.ReturnValue = _cache.Retrieve(cacheKey);
                return;
            }

            // Method result is not found in cache, proceed with method.
            invocation.Proceed();

            if (cacheKey != null)
                SaveInCache(cacheKey, invocation.ReturnValue);
        }

        private void SaveInCache(string key, object returnValue)
        {
            lock (_cache)
            {
                _cache.Add(returnValue, key);
            }
        }

        private string GenerateCacheKey(IInvocation invocation)
        {
            var methodName = invocation.Method.Name;
            var methodArgs = GetArgumentsAsStrings(invocation.Arguments);

            //if (AllArgumentsAreCacheable(methodArgs) == false)
            //    return null;

            var argsHashCodes = methodArgs.ToArray();
            return _cacheKeyGenerator.GenerateCacheKeyForMethod(methodName, argsHashCodes);
        }

        private static IEnumerable<string> GetArgumentsAsStrings(IEnumerable<object> methodArgs)
        {
            return methodArgs.Select(GetArgumentAsString).ToList();
        }

        private static string GetArgumentAsString(object arg)
        {
            if (arg is ICacheable)
                return (arg as ICacheable).Key;

            if (arg is string)
                return arg as string;

            return null;
        }

        private static bool AllArgumentsAreCacheable(IEnumerable<object> arguments)
        {
            return arguments.All(arg => arg is ICacheable);
        }

        private static bool IsMethodCachable(MethodInfo method)
        {
            return method.GetCustomAttributes(true).Any(x => x.GetType() == typeof(CachedAttribute));
        }
    }
}
