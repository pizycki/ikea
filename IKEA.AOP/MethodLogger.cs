﻿using System.Collections.Generic;
using System.Linq;
using Castle.DynamicProxy;
using IKEA.Common.Contracts.Application;
using NLog;

namespace IKEA.AOP
{
    public class MethodLogger : IMethodLogger, IInterceptor
    {
        readonly Logger _log = LogManager.GetCurrentClassLogger();

        public void Intercept(IInvocation invocation)
        {
            string methodName = invocation.Method.Name;
            var args = invocation.Arguments.Select(a => (a ?? "").ToString());

            LogEnter(methodName, args);

            invocation.Proceed();

            LogExit(methodName, invocation.ReturnValue);
        }

        public void LogEnter(string methodName, IEnumerable<string> args)
        {
            var argumentsAsSingleString = string.Join("; ", args);
            _log.Debug("Entering method: {0} | Args: {1}", methodName, argumentsAsSingleString);
        }

        public void LogExit(string methodName, object returnValue)
        {
            _log.Debug("Leaving method: {0} | Result: {1}", methodName, returnValue);
        }
    }
}
