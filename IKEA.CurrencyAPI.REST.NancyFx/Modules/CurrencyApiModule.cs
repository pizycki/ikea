﻿using IKEA.Common.Contracts.Application;

using Nancy;
using System.Linq;

namespace IKEA.CurrencyAPI.REST.NancyFx.Modules
{
    public class CurrencyApiModule : NancyModule
    {
        private readonly ICurrencyServer _currencyRateSrv;

        public CurrencyApiModule(ICurrencyServer currencyRateSrv)
        {
            _currencyRateSrv = currencyRateSrv;

            Get[@"/currencies/list"] = _ =>
            {
                var result = _currencyRateSrv.GetCurrencies();

                return Response.AsJson(result);
            };

            Get[@"/currencies/dictionary"] = _ =>
            {
                var result = _currencyRateSrv.GetCurrencyDictionary();

                return Response.AsJson(result);
            };

            Get[@"/currencies/abbreviation"] = _ =>
            {
                var result = _currencyRateSrv.GetCurrencyAbbreviations();

                return Response.AsJson(result);
            };

            Get[@"currencies/ratio/{baseAbrv}/to/{targetAbrv}/"] = _ =>
            {
                var currencies = _currencyRateSrv.GetCurrencies();

                var baseCurr = currencies.Single(x => x.Abbreviation == _.baseAbrv);

                var targetCurr = currencies.Single(x => x.Abbreviation == _.targetAbrv);

                var result = _currencyRateSrv.GetCurrencyRatio(baseCurr,targetCurr);

                return Response.AsJson(result);
            };
        }
    }
}
