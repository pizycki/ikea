﻿using System;
using System.Linq;
using System.Collections.Generic;
using IKEA.Common.Attributes.Caching;
using IKEA.Common.Exceptions;
using IKEA.Common.Utils;
using NLog;
using IKEA.Common.Contracts.POCO;
using IKEA.Common.Contracts.DataProvider;
using IKEA.Common.Contracts.DAL;
using IKEA.Common.Contracts.Application;

namespace IKEA.Application
{
    public class CurrencyServer : ICurrencyServer
    {
        private Logger Log
        {
            get { return _logSrv; }
        }
        public TimeSpan DbCurrencyRatioExpirationTime { get; private set; }

        private readonly ICurrencyRepository _currenciesRepo;
        private readonly ICurrencyRatioRepository _currencyRatiosRepo;
        private readonly ICurrencyDataProvider _currencyDataProvider;
        private readonly Logger _logSrv;

        #region [ Constructors ]
        public CurrencyServer(ICurrencyRepository currRepo, ICurrencyRatioRepository currRatioRepo, ICurrencyDataProvider currencyDataProvider)
        {
            _logSrv = LogManager.GetCurrentClassLogger();
            _currenciesRepo = currRepo;
            _currencyRatiosRepo = currRatioRepo;
            _currencyDataProvider = currencyDataProvider;
            DbCurrencyRatioExpirationTime = new TimeSpan(long.Parse(ConfigGetter.GetValueFor(Constants.DefaultDbCurrencyRatioExpirationTimeConfigKey)));
        }

        #endregion

        #region [ API methods ]

        [Cached]
        public virtual ICurrencyRatio GetCurrencyRatio(ICurrency ratioBase,
            ICurrency ratioTarget)
        {
            // Check if both currencies are supported
            if (IsCurrencySupported(ratioBase) == false
                || IsCurrencySupported(ratioTarget) == false)
            {
                Log.Error(new UnsupportedCurrencyException());
                return null;
            }

            // Try get ratio from repository
            var latestRatio = GetLatestCurrencyRatioFromRepository(ratioBase, ratioTarget);

            // If not found or obsolete, get from data provider and update database
            if (latestRatio == null || !IsRatioCurrent(latestRatio))
            {
                Log.Debug("Updating currency ratio for base: {0} and target: {1}",
                    ratioBase.Abbreviation, ratioTarget.Abbreviation);
                latestRatio = GetCurrencyRatioViaDataProvider(ratioBase, ratioTarget);
                UpdateRepositoryWithNewRatio(latestRatio);
            }

            return latestRatio;
        }

        public virtual IEnumerable<string> GetCurrencyAbbreviations()
        {
            return _currenciesRepo.GetAll().Select(c => c.Abbreviation);
        }

        public virtual IEnumerable<ICurrency> GetCurrencies()
        {
            return _currenciesRepo.GetAll();
        }

        public virtual IDictionary<string, ICurrency> GetCurrencyDictionary()
        {
            return _currenciesRepo.GetAll().ToDictionary(c => c.Abbreviation);
        }

        public float? ConvertCurrency(ICurrency baseCurrency, float baseCurrencyValue, ICurrency targetCurrency)
        {
            var ratio = GetCurrencyRatio(baseCurrency, targetCurrency);
            return ratio.Value * baseCurrencyValue;
        }

        #endregion

        #region [ Encapsulated methods ]
        protected virtual ICurrencyRatio GetLatestCurrencyRatioFromRepository(ICurrency baseCurrency, ICurrency targetCurrency)
        {
            return _currencyRatiosRepo.GetSingle(baseCurrency.Abbreviation, targetCurrency.Abbreviation);
        }

        protected virtual ICurrencyRatio GetCurrencyRatioViaDataProvider(ICurrency baseCurrency, ICurrency targetCurrency)
        {
            return _currencyDataProvider.GetCurrencyRatio(baseCurrency, targetCurrency);
        }

        protected virtual void UpdateRepositoryWithNewRatio(ICurrencyRatio ratio)
        {
            _currencyRatiosRepo.Add(ratio);
        }

        protected virtual bool IsRatioCurrent(ICurrencyRatio ratio)
        {
            return DateTime.Now - ratio.CreationTime < DbCurrencyRatioExpirationTime == false;
        }

        [Cached]
        protected virtual bool IsCurrencySupported(ICurrency seekedCurrency)
        {
            var foundCurrency = _currenciesRepo.GetByAbbreviation(seekedCurrency.Abbreviation);
            return foundCurrency != null;
        }

        #endregion


        private static class Constants
        {
            public const string DefaultDbCurrencyRatioExpirationTimeConfigKey = "DbCurrencyRatioExpirationTime";
        }
    }
}