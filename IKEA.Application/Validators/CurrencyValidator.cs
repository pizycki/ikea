﻿using IKEA.Common.Contracts.Application.Validators;
using IKEA.Common.Contracts.POCO;

namespace IKEA.Application.Validators
{
    public class CurrencyValidatorFactory : ICurrencyValidatorFactory
    {
        public ICurrencyValidator Create()
        {
            return new CurrencyValidator();
        }
    }

    class CurrencyValidator : ICurrencyValidator
    {
        public bool Validate(ICurrency currency, out string error)
        {
            error = string.Empty;

            if (!ValidateName(currency.Name))
            {
                error = "Currency name is not valid. Check if it's not too short or too long";
                return false;
            }

            if (!ValidateAbbrevation(currency.Abbreviation))
            {
                error = "Currency seekedCurrency is not valid. Check if it contains three big letters only.";
                return false;
            }

            return true;
        }

        protected virtual bool ValidateAbbrevation(string abbr)
        {
            return abbr != null
                && CheckAbbreviationLength(abbr)
                && ContainsOnlyBigLetters(abbr);
        }

        protected bool CheckAbbreviationLength(string abbr)
        {
            return abbr.Length == Constants.ABBREVIATION_LENGTH;
        }

        protected bool ContainsOnlyBigLetters(string abbr)
        {
            foreach (char c in abbr)
                if (char.IsLetter(c) && !char.IsUpper(c))
                    return false;
            return true;
        }

        protected virtual bool ValidateName(string name)
        {
            return name != null && CheckNameLength(name);
        }

        protected bool CheckNameLength(string name)
        {
            return name.Length < Constants.NAME_MAX_LENGTH
               && name.Length > Constants.NAME_MIN_LENGTH;
        }

        class Constants
        {
            public const int NAME_MAX_LENGTH = 100;
            public const int NAME_MIN_LENGTH = 3;
            public const int ABBREVIATION_LENGTH = 3;
        }

    }
}
