﻿using System;
using IKEA.Common.Contracts.Application.Validators;
using IKEA.Common.Contracts.POCO;

namespace IKEA.Application.Validators
{
    public class CurrencyRatioValidatorFactory : ICurrencyRatioValidatorFactory
    {
        readonly ICurrencyValidatorFactory _currValFactory;
        public CurrencyRatioValidatorFactory(ICurrencyValidatorFactory currValFactory)
        {
            _currValFactory = currValFactory;
        }

        public ICurrencyRatioValidator Create()
        {
            var validator = new CurrencyRatioValidator(_currValFactory.Create());
            return validator;
        }
    }

    class CurrencyRatioValidator : ICurrencyRatioValidator
    {
        readonly ICurrencyValidator _currencyValidator;

        public CurrencyRatioValidator(ICurrencyValidator currencyValidator)
        {
            _currencyValidator = currencyValidator;
        }

        public bool Validate(ICurrencyRatio currencyRatio, out string error)
        {
            if (!ValidateBase(currencyRatio.Base, out error))
            {
                if (error == string.Empty)
                {
                    error = "Base seekedCurrency is not valid";
                }
                return false;
            }

            if (!ValidateTarget(currencyRatio.Target, out error))
            {
                if (error == string.Empty)
                {
                    error = "Target seekedCurrency is not valid";
                }
                return false;
            }

            if (currencyRatio.Value != null && !ValidateRatioValue(currencyRatio.Value.Value))
            {
                error = "Ratio value is not valid";
                return false;
            }

            return true;
        }

        protected virtual bool ValidateRatioValue(float ratioValue)
        {
            return IsRatioValuePositive(ratioValue);
        }

        protected bool IsRatioValuePositive(float ratioValue)
        {
            return ratioValue > 0;
        }

        protected virtual bool ValidateTarget(ICurrency targetCurrency, out string error)
        {
            error = string.Empty;
            return targetCurrency != null && ValidateCurrency(targetCurrency, out error);
        }

        protected virtual bool ValidateId(int id)
        {
            return IsIdPositiveNumber(id);
        }

        protected bool IsIdPositiveNumber(int id)
        {
            return id > 0;
        }

        protected virtual bool ValidateBase(ICurrency currencyBase, out string error)
        {
            error = string.Empty;
            return currencyBase != null && ValidateCurrency(currencyBase, out error);
        }

        protected bool ValidateCurrency(ICurrency currency, out string error)
        {
            return _currencyValidator.Validate(currency, out error);
        }
    }
}
