﻿using System;
using System.Threading.Tasks;
using System.Timers;

namespace IKEA.Application
{
    public class Tasker
    {
        Func<bool> _function;

        public Tasker(Func<bool> func, int span, Spantype type)
        {
            Task.Factory.StartNew(() => Conf(func, span, type));
        }

        public void Conf(Func<bool> func, int span, Spantype type)
        {
            var timer = new Timer();

            if (type == Spantype.seconds)
                timer.Interval = span * 1000;

            if (type == Spantype.minutes)
                timer.Interval = span * 60000; // 1000 * 60

            if (type == Spantype.hours)
                timer.Interval = span * 3600000; // 1000 * 60 * 60 

            _function = func;

            timer.Elapsed += OnTimerElapsed;

            timer.Start();
        }

        void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            _function();
        }

        public enum Spantype
        {
            seconds,
            minutes,
            hours
        }
    }
}
