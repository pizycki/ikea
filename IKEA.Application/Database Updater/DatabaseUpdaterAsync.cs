﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IKEA.Common.Contracts.Application;
using IKEA.Common.Contracts.DAL;
using IKEA.Common.Contracts.DataProvider;
using IKEA.Common.Contracts.POCO;
using IKEA.Common.Models.Domain;
using NLog;

namespace IKEA.Application
{
    public class DatabaseUpdaterAsync : IRunnable
    {
        readonly Logger Log = LogManager.GetCurrentClassLogger();
        readonly ICurrencyDataProvider _currencyProvider;
        readonly ICurrencyRepository _currencyRepository;
        readonly ICurrencyRatioRepository _currencyRatioRepository;

        readonly int _daysToUpdateCurrencyList;
        readonly int _minutesToUpdateRatioList;

        readonly Task _updateCurrencyListTask;
        readonly Task _updateRatioListTask;

        readonly object _locker = new object();

        readonly string[] _popularCurrencies;

        bool _firstRun;

        public DatabaseUpdaterAsync(
            ICurrencyDataProvider currencyProvider,
            ICurrencyRepository currencyRepository,
            ICurrencyRatioRepository currencyRatioRepository,
            int daysToUpdateCurrencyList,
            int minutesToUpdateCurrencyRatioList,
            string[] popularCurrencies
            )
        {
            _currencyProvider = currencyProvider;
            _currencyRepository = currencyRepository; 
            _currencyRatioRepository = currencyRatioRepository;

            _daysToUpdateCurrencyList = daysToUpdateCurrencyList;
            _minutesToUpdateRatioList = minutesToUpdateCurrencyRatioList;

            _updateCurrencyListTask = new Task(UpdateCurrencyList);
            _updateRatioListTask = new Task(UpdateRatioList);

            _popularCurrencies = popularCurrencies;

            _firstRun = true;
        }

        private async void UpdateCurrencyList()
        {
            while (true)
            {
                lock (_locker)
                {
                    Log.Debug("Currencies list update start.");

                    var currencyList = _currencyProvider.GetAvaibleCurrencies();
                    var actualList = _currencyRepository.GetAll();

                    foreach (var item in currencyList)
                    {
                        if (actualList.Any(x => x.Abbreviation == item.Abbreviation)) 
                            continue;

                        var currency = new Currency(item);
                        _currencyRepository.Add(currency);
                    }
                    Log.Debug("Currencies list update end.");

                    if (_firstRun)
                        _firstRun = false;
                }

                await DelayToUpdateCurrencyList();
            }
        }

        private async Task<IEnumerable<ICurrencyRatio>> DownloadRatioList()
        {
            await Task.Delay(1);

            var currencyList = _currencyRepository.GetAll();

            var resultRatioList = new List<ICurrencyRatio>();

            for (int i = 0; i < _popularCurrencies.Length; i++)
            {
                var ratioBase = currencyList.SingleOrDefault(x => x.Abbreviation == _popularCurrencies[i]);
                for (int j = 0; j < _popularCurrencies.Length; j++)
                {
                    ICurrency ratioTarget = currencyList.SingleOrDefault(x => x.Abbreviation == _popularCurrencies[j]);

                    Log.Debug("{0} - {1}", ratioBase.Abbreviation, ratioTarget.Abbreviation);

                    var ratio = _currencyProvider.GetCurrencyRatio(ratioBase, ratioTarget);

                    var ratioToAdd = new CurrencyRatio()
                    {
                        Base = ratio.Base,
                        Target = ratio.Target,
                        Value = ratio.Value.Value,
                        CreationTime = DateTime.Now
                    };

                    resultRatioList.Add(ratioToAdd);
                }
            }

            return resultRatioList;
        }

        private async Task DelayToUpdateRatio()
        {
            for (int i = 0; i < _minutesToUpdateRatioList; i++)
            {
                Log.Debug(String.Format("Next ratio update in {0} minutes", 30 - i));
                await Task.Delay(TimeSpan.FromMinutes(1));
            }
        }

        private async Task DelayToUpdateCurrencyList()
        {
            var value = _daysToUpdateCurrencyList * 24;

            for (int i = 0; i < value; i++)
            {
                Log.Debug(String.Format("Next curr update in {0} hours", 24 - i));
                await Task.Delay(TimeSpan.FromHours(1));
            }


        }

        private async void UpdateRatioList()
        {
            while (true)
            {
                if (_firstRun)
                    continue; 

                var list = await DownloadRatioList();

                _currencyRatioRepository.Add(list.ToArray());

                await DelayToUpdateRatio();
            }
        }

        public void Start()
        {
            _updateCurrencyListTask.Start();
            _updateRatioListTask.Start();
        }

        public void Stop()
        {
            _updateCurrencyListTask.Wait();
            _updateRatioListTask.Wait();
        }

        public bool Running
        {
            get { throw new NotImplementedException(); }
        }
    }
}
