#r "IKEA.FCCA.Client.dll"
#r "IKEA.POCO.dll"
#r "IKEA.Contracts.dll"

//GetAvaibleCurrencies

var cdp = new IKEA.FCCA.Client.CurrencyDataProvider();

var currencies = cdp.GetAvaibleCurrencies();

foreach(var curr in currencies)
{
	Console.WriteLine("{0}, {1}, {2}", curr.Name, curr.Abbreviation, curr.Country);
}