#r "IKEA.FCCA.Client.dll"
#r "IKEA.POCO.dll"
#r "IKEA.Contracts.dll"

var cdp = new IKEA.FCCA.Client.CurrencyDataProvider();

var currRatio = cdp.GetCurrencyRatio(
	new IKEA.POCO.Currency("", "USD", "") , 
	new IKEA.POCO.Currency("","PLN", "") 
);

Console.WriteLine(currRatio.Value);
