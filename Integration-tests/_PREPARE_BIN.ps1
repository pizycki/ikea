﻿$SourceDirectory =   'C:\Users\Marcin\Documents\ikea\'
$DestinationDirectory = 'C:\Users\Marcin\Documents\ikea\Integration-tests\bin\'


# Wipe all old files from 'bin' directory
Remove-Item $DestinationDirectory -ErrorAction SilentlyContinue -Force -Confirm:$false

# Create new directory
New-item -ItemType directory -Path $DestinationDirectory

# Copy new files
$Include = @("*.dll*")

$files = Get-ChildItem $SourceDirectory -Recurse -Include $Include

clear
foreach($f in $files)
{
    $src = $f.FullName
    $dest = $DestinationDirectory + $f.Name
    
    echo "====================================="
    if(!(Test-Path($dest)))
    {
        echo ("Copying: " + $src + " to: " + $dest)
        $f.CopyTo($dest)
    }
    else
    {
        echo "File already exists!"
        echo $f.FullName
        echo "Skipping..."
    }
}