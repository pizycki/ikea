﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace IKEA.Common.Utils
{
    #region [ Contracts ]
    public interface ISimpleMapper<TSource, TDest>
    {
        void AddMapping(Action<TSource, TDest> mapping);
        TDest MapObject(TSource source, TDest dest);
    }

    public interface IMapperWithObjectCreation<TSource, TDest> : ISimpleMapper<TSource, TDest> where TDest : new()
    {
        TDest CreateMappedObject(TSource source);
    }

    #endregion

    public static class StaticMapper
    {
        public static void Map<TSource, TTarget>(TSource source, TTarget target)
        {
            var mapper = new SimpleMapper<TSource, TTarget>();
            mapper.MapObject(source, target);
        }

        public static TTarget CreateAndMap<TSource, TTarget>(TSource source)
            where TTarget : new()
        {
            var mapper = new MapperWithObjectCreation<TSource, TTarget>();
            return mapper.CreateMappedObject(source);
        }
    }

    public class SimpleMapper<TSource, TDest> : ISimpleMapper<TSource, TDest>
    {
        private readonly IList<Action<TSource, TDest>> mappings = new List<Action<TSource, TDest>>();

        private void CopyMatchingProperties(TSource source, TDest dest)
        {
            foreach (var destProp in typeof(TDest)
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(p => p.CanWrite))
            {
                var sourceProp = typeof(TSource)
                    .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    .FirstOrDefault(p => p.Name == destProp.Name && p.PropertyType == destProp.PropertyType);
                if (sourceProp != null)
                    destProp.SetValue(dest, sourceProp.GetValue(source, null), null);
            }
        }

        public virtual void AddMapping(Action<TSource, TDest> mapping)
        {
            mappings.Add(mapping);
        }

        public virtual TDest MapObject(TSource source, TDest dest)
        {
            CopyMatchingProperties(source, dest);
            foreach (var action in mappings)
                action(source, dest);
            return dest;
        }
    }

    public class MapperWithObjectCreation<TSource, TDest> : SimpleMapper<TSource, TDest>, IMapperWithObjectCreation<TSource, TDest>
        where TDest : new()
    {
        public virtual TDest CreateMappedObject(TSource source)
        {
            var dest = new TDest();
            return MapObject(source, dest);
        }
    }
}
