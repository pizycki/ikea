﻿using System;
using System.Linq.Expressions;

namespace IKEA.Common.Utils
{
    public class ReflectionUtils
    {
        public static string GetAllMembersAndPropertyName<T>(Expression<Func<T>> propertyLambda)
        {
            var memberExpr = propertyLambda.Body as MemberExpression;

            if (memberExpr == null)
                throw new ArgumentException("You must pass a lambda of the form: '() => Class.Property' or '() => object.Property'");

            var result = string.Empty;
            do
            {
                result = memberExpr.Member.Name + "." + result;
                memberExpr = memberExpr.Expression as MemberExpression;
            } while (memberExpr != null);

            result = result.Remove(result.Length - 1); // remove the trailing "."
            return result;
        }

        public static string GetPropertyName<T>(Expression<Func<T>> propertyLambda)
        {
            var fullPropertyName = GetAllMembersAndPropertyName<T>(propertyLambda);
            var elements = fullPropertyName.Split('.');
            var numberOfElements = elements.Length;
            return elements[numberOfElements - 1]; // Return last one.
        }
    }
}
