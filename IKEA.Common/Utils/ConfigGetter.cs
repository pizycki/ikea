﻿using System.Configuration;

namespace IKEA.Common.Utils
{
    public static class ConfigGetter
    {
        public static string GetValueFor(string configKey)
        {
            return ConfigurationManager.AppSettings[configKey];
        }
    }
}
