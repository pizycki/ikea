﻿using System;
using IKEA.Common.Contracts.POCO;

namespace IKEA.Common.Exceptions
{
    [Serializable]
    public class UnsupportedCurrencyException : ServerException
    {
        public UnsupportedCurrencyException()
            : base()
        {
        }

        public ICurrency Currency { get; set; }
    }
}
