﻿using System;

namespace IKEA.Common.Exceptions
{
    public class ServerException : Exception
    {
        public ServerException() { }

        public ServerException(string message)
            : base(message) { }

        public ServerException(string message, Exception innerExc)
            : base(message, innerExc) { }
    }
}
