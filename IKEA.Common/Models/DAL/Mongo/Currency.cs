﻿using IKEA.Common.Contracts.POCO;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace IKEA.Common.Models.DAL.Mongo
{
    public class Currency : ICurrency
    {
        [BsonId]
        public ObjectId CurrencyId { get; set; }
        public string Name { get; private set; }
        public string Abbreviation { get; private set; }
        public string Country { get; private set; }
    }
}
