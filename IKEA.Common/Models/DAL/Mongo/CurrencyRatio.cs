﻿using System;
using IKEA.Common.Contracts.POCO;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace IKEA.Common.Models.DAL.Mongo
{
    public class CurrencyRatio : ICurrencyRatio
    {
        [BsonId]
        public ObjectId CurrencyRatioId { get; set; }

        public ICurrency Base { get; set; }

        public ICurrency Target { get; set; }

        public float? Value { get; set; }

        public DateTime? CreationTime { get; set; }
    }
}
