﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IKEA.Common.Contracts.POCO;

namespace IKEA.Common.Models.DAL.EF
{
    [Table(SqlSchema.CurrencyRatios.TableName)]
    public class CurrencyRatio : ICurrencyRatio
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(SqlSchema.CurrencyRatios.ColumnNames.CurrencyRatioId)]
        public int CurrencyRatioId { get; set; }

        [Required]
        [Column(SqlSchema.CurrencyRatios.ColumnNames.Value)]
        public float? Value { get; set; }

        [Required]
        [Column(SqlSchema.CurrencyRatios.ColumnNames.CreationTime)]
        public DateTime? CreationTime { get; set; }

        [ForeignKey(SqlSchema.CurrencyRatios.ForeignKeys.BaseId)]
        public virtual Currency Base { get; set; }
        public int? CurrencyRatioBaseId { get; set; }

        [ForeignKey(SqlSchema.CurrencyRatios.ForeignKeys.TargetId)]
        public virtual Currency Target { get; set; }
        public int? CurrencyRatioTargetId { get; set; }

        #region [ NotMapped ]
        [NotMapped]
        ICurrency ICurrencyRatio.Base
        {
            get { return Base; }
            set { Base = (Currency)value; }
        }

        [NotMapped]
        ICurrency ICurrencyRatio.Target
        {
            get { return Target; }
            set { Target = (Currency)value; }
        }
        #endregion

    }
}
