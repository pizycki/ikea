﻿namespace IKEA.Common.Models.DAL.EF
{
    public static class SqlSchema
    {
        public static class Currencies
        {
            public const string TableName = "Currencies";

            public static class ColumnNames
            {
                public const string CurrencyId = "CurrencyId";
                public const string Name = "CurrencyName";
                public const string Abbreviation = "CurrencyAbbreviation";
                public const string Country = "CurrencyCountry";
            }
        }

        public static class CurrencyRatios
        {
            public const string TableName = "CurrencyRatios";

            public static class ColumnNames
            {
                public const string CurrencyRatioId = "CurrencyRatioId";
                public const string Value = "CurrencyRatioValue";
                public const string CreationTime = "CurrencyRatioCreationTime";
                public const string CurrencyRatioBaseId = "CurrencyRatioBaseId";
                public const string CurrencyRatioTargetId = "CurrencyRatioTargetId";
            }

            public static class ForeignKeys
            {
                public const string BaseId = ColumnNames.CurrencyRatioBaseId;
                public const string TargetId = ColumnNames.CurrencyRatioTargetId;
            }
        }
    }
}
