﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IKEA.Common.Contracts.POCO;
using IKEA.Common.Models.DAL;

namespace IKEA.Common.Models.DAL.EF
{
    [Table(SqlSchema.Currencies.TableName)]
    public class Currency : ICurrency
    {
        [Key]
        [Required]
        [Column(SqlSchema.Currencies.ColumnNames.CurrencyId)]
        public int CurrencyId { get; set; }

        [Required]
        [StringLength(100)]
        [Column(SqlSchema.Currencies.ColumnNames.Name)]
        public string Name { get; set; }

        [Required]
        [StringLength(3, MinimumLength = 3)]
        [Column(SqlSchema.Currencies.ColumnNames.Abbreviation)]
        public string Abbreviation { get; set; }

        [StringLength(50)]
        [Column(SqlSchema.Currencies.ColumnNames.Country)]
        public string Country { get; set; }
    }
}
