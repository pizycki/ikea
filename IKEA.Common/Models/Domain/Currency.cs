﻿using IKEA.Common.Contracts.POCO;
using IKEA.Common.Utils;

namespace IKEA.Common.Models.Domain
{
    public class Currency : ICurrency, ICacheable
    {
        public Currency(ICurrency currency)
        {
            StaticMapper.Map(currency, this);
        }

        public Currency(string name, string abbrv, string country)
        {
            Name = name;
            Abbreviation = abbrv;
            Country = country;
        }

        public string Name { get; private set; }
        public string Abbreviation { get; private set; }
        public string Country { get; private set; }

        public string Key
        {
            get { return Abbreviation; }
        }
    }
}
