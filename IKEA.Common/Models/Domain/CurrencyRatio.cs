﻿using IKEA.Common.Contracts.POCO;
using System;

namespace IKEA.Common.Models.Domain
{
    public class CurrencyRatio : ICurrencyRatio
    {
        public ICurrency Base { get; set; }
        public ICurrency Target { get; set; }
        public DateTime? CreationTime { get; set; }
        public float? Value { get; set; }
    }
}
