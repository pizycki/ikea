﻿namespace IKEA.Common.Contracts.Domain.Cache
{
    public interface IMethodCacheKeyGenerator
    {
        string GenerateCacheKeyForMethod(string methodName, params string[] parameterName);
    }
}
