﻿using System.Collections.Generic;
using IKEA.Common.Contracts.POCO;

namespace IKEA.Common.Contracts.API
{
    public interface ICurrencyRateService
    {
        ICurrencyRatio GetCurrencyRatio(ICurrency baseCurrency, ICurrency targetCurrency);

        IEnumerable<string> GetCurrencyAbbreviations();

        IEnumerable<ICurrency> GetCurrencies();

        IDictionary<string, ICurrency> GetCurrencyDictionary();

        float? ConvertCurrency(ICurrency baseCurrency, float baseCurrencyValue, ICurrency targetCurrency);
    }
}