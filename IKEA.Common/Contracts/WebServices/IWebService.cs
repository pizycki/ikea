﻿using IKEA.Common.Contracts.Application;

namespace IKEA.Common.Contracts.WebServices
{
    public interface IWebService : IRunnable
    {
        void Setup();
        string Address { get; }
    }
}
