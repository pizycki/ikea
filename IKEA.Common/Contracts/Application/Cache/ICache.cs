﻿namespace IKEA.Common.Contracts.Application.Cache
{
    public interface ICache
    {
        bool Has(string key);
        object Retrieve(string key);
        void Add(object item, string key);
    }
}
