﻿namespace IKEA.Common.Contracts.Application.Cache
{
    public interface IMethodCacheKeyGenerator
    {
        string GenerateCacheKeyForMethod(string methodName, params string[] parameterName);
    }
}
