﻿using System.Collections.Generic;

namespace IKEA.Common.Contracts.Application
{
    public interface IMethodLogger
    {
        void LogEnter(string methodName, IEnumerable<string> args);
        void LogExit(string methodName, object returnValue);
    }
}
