﻿namespace IKEA.Common.Contracts.Application
{
    public interface IRunnable
    {
        void Start();
        void Stop();
        bool Running { get; }
    }
}
