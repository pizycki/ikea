﻿using IKEA.Common.Contracts.POCO;

namespace IKEA.Common.Contracts.Application.Validators
{
    public interface ICurrencyRatioValidator
    {
        bool Validate(ICurrencyRatio currencyRatio, out string error);
    }

    public interface ICurrencyRatioValidatorFactory
    {
        ICurrencyRatioValidator Create();
    }
}
