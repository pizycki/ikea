﻿using IKEA.Common.Contracts.POCO;

namespace IKEA.Common.Contracts.Application.Validators
{
    public interface ICurrencyValidator
    {
        bool Validate(ICurrency currency, out string error);
    }

    public interface ICurrencyValidatorFactory
    {
        ICurrencyValidator Create();
    }
}
