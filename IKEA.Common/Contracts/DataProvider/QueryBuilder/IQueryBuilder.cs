﻿namespace IKEA.Common.Contracts.DataProvider.QueryBuilder
{
    public interface IQueryBuilder
    {
        T Create<T>() where T : class, IQuery;
    }

    public interface IQuery
    {
        string Query { get; }
    }
}
