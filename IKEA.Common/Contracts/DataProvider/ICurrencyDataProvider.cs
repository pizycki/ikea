﻿using System.Collections.Generic;
using IKEA.Common.Contracts.POCO;

namespace IKEA.Common.Contracts.DataProvider
{
    public interface ICurrencyDataProvider
    {
        IEnumerable<ICurrency> GetAvaibleCurrencies();
        ICurrencyRatio GetCurrencyRatio(ICurrencyRatio currencyRatio);
        void GetCurrencyRatio(ref ICurrencyRatio currencyRatio);
        ICurrencyRatio GetCurrencyRatio(ICurrency baseCurrency, ICurrency targetCurrency);
        ICurrencyRatio[] GetCurrencyRatios(ICurrencyRatio[] currencyRatios);
    }
}
