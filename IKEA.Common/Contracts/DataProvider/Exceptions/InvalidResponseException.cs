﻿using IKEA.Common.Exceptions;

namespace IKEA.Common.Contracts.DataProvider.Exceptions
{
    public class InvalidResponseException : ServerException
    {
    }
}
