﻿using IKEA.Common.Exceptions;

namespace IKEA.Common.Contracts.DataProvider.Exceptions
{
    public abstract class QueryBuilderException : ServerException
    {
    }
}
