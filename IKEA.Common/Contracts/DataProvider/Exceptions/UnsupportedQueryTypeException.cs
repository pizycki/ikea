﻿using System;

namespace IKEA.Common.Contracts.DataProvider.Exceptions
{
    public class UnsupportedQueryTypeException : QueryBuilderException
    {
        public Type QueryType { get; set; }
    }
}
