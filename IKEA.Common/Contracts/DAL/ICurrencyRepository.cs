﻿using System.Collections.Generic;
using IKEA.Common.Contracts.POCO;

namespace IKEA.Common.Contracts.DAL
{
    public interface ICurrencyRepository
    {
        ICurrency GetByAbbreviation(string abbreviation);
        IEnumerable<ICurrency> GetManyByAbbreviation(IEnumerable<string> abbreviations);
        IEnumerable<ICurrency> GetAll();
        ICurrency GetSingle(int id);
        void Add(params ICurrency[] items);
    }
}
