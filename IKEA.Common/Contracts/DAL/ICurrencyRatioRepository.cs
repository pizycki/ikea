﻿using System.Collections.Generic;
using IKEA.Common.Contracts.POCO;

namespace IKEA.Common.Contracts.DAL
{
    public interface ICurrencyRatioRepository
    {
        ICurrencyRatio GetSingle(string ratioBaseAbbreviation, string ratioTargetAbbreviation);
        void Add(params ICurrencyRatio[] ratio);
    }
}