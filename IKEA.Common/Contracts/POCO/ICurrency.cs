﻿namespace IKEA.Common.Contracts.POCO
{
    public interface ICurrency
    {
        string Name { get; }
        string Abbreviation { get; }
        string Country { get; }
    }
}
