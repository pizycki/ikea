﻿namespace IKEA.Common.Contracts.POCO
{
    public interface ICacheable
    {
        string Key { get; }
    }
}
