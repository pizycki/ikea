﻿using System;

namespace IKEA.Common.Contracts.POCO
{
    public interface ICurrencyRatio
    {
        ICurrency Base { get; set; }
        ICurrency Target { get; set; }
        float? Value { get; set; }
        DateTime? CreationTime { get; set; }
    }
}
