﻿using System;

namespace IKEA.Common.Attributes.Caching
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Field)]
    public class CachedAttribute : Attribute { }
}
