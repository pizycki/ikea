﻿namespace IKEA.CurrencyAPI.SOAP
{
    public static class Metadata
    {
        public const string ModelsNamespace = "IKEA.CurrencyAPI.SOAP.Models.Domain";
        public const string ServicesNamespace = "IKEA.CurrencyAPI.SOAP.Common";
    }
}
