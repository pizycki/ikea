﻿using IKEA.Common.Contracts.POCO;
using System;
using System.Runtime.Serialization;

namespace IKEA.CurrencyAPI.SOAP.Models
{
    [DataContract(Namespace = Metadata.ModelsNamespace)]
    [KnownType(typeof(CurrencyInfo))]
    public class CurrencyRatioInfo : ICurrencyRatio, ICacheable
    {
        [DataMember]
        public ICurrency Base { get; set; }

        [DataMember]
        public ICurrency Target { get; set; }

        [DataMember]
        public float? Value { get; set; }

        [DataMember]
        public DateTime? CreationTime { get; set; }

        public string Key
        {
            get { return string.Format("{0}-{1}", Base.Abbreviation, Target.Abbreviation); }
        }
    }
}
