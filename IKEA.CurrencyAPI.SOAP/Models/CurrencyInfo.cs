﻿using IKEA.Common.Contracts.POCO;
using System.Runtime.Serialization;

namespace IKEA.CurrencyAPI.SOAP.Models
{
    [DataContract(Namespace = Metadata.ModelsNamespace)]
    public class CurrencyInfo : ICurrency, ICacheable
    {
        public CurrencyInfo()
        {
        }

        public CurrencyInfo(string name, string abbrv, string country)
        {
            Name = name;
            Abbreviation = abbrv;
            Country = country;
        }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Abbreviation { get; set; }

        [DataMember]
        public string Country { get; set; }

        public string Key
        {
            get { return Abbreviation; }
        }
    }
}
