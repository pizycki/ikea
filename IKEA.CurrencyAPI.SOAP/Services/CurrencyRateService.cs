﻿using System.ServiceModel;
using System.Linq;
using System.Collections.Generic;
using IKEA.Common.Utils;
using NLog;
using IKEA.CurrencyAPI.SOAP.Models;
using IKEA.Common.Exceptions;
using IKEA.Common.Contracts.POCO;
using ICurrencyRateService = IKEA.CurrencyAPI.SOAP.Contracts.ICurrencyRateService;

namespace IKEA.CurrencyAPI.SOAP.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class CurrencyRateService : ICurrencyRateService
    {
        private readonly Common.Contracts.API.ICurrencyRateService _currencyRateSrv;
        private Logger Log { get; set; }

        public CurrencyRateService(Common.Contracts.API.ICurrencyRateService currencyRateSrv)
        {
            Log = LogManager.GetCurrentClassLogger();
            _currencyRateSrv = currencyRateSrv;
        }

        public CurrencyRatioInfo GetCurrencyRatio(CurrencyInfo baseCurrency, CurrencyInfo targetCurrency)
        {
            Log.Debug("Getting currency ratio for {0} - {1}", baseCurrency.Abbreviation, targetCurrency.Abbreviation);
            ICurrencyRatio result;
            try
            {
                result = _currencyRateSrv.GetCurrencyRatio(baseCurrency, targetCurrency);
            }
            catch (UnsupportedCurrencyException ex)
            {
                Log.Error(ex);
                result = null;
            }

            // Konstruktory...

            return result != null ? MapToCurrencyRatioInfo(result) : null;
        }

        public IEnumerable<CurrencyInfo> GetCurrencies()
        {
            Log.Debug("Getting all currencies list.");
            var result = _currencyRateSrv.GetCurrencies();
            return result.Select(StaticMapper.CreateAndMap<ICurrency, CurrencyInfo>);
        }

        public IDictionary<string, CurrencyInfo> GetCurrencyDictionary()
        {
            Log.Debug("Getting currencies dictionary.");
            return GetCurrencies().ToDictionary(x => x.Abbreviation, y => y);
        }

        public float? ConvertCurrency(CurrencyInfo baseCurrency, float baseCurrencyValue, CurrencyInfo targetCurrency)
        {
            return _currencyRateSrv.ConvertCurrency(baseCurrency, baseCurrencyValue, targetCurrency);
        }

        private CurrencyRatioInfo MapToCurrencyRatioInfo(ICurrencyRatio source)
        {
            var ratioInfo = new MapperWithObjectCreation<ICurrencyRatio, CurrencyRatioInfo>()
                .CreateMappedObject(source);

            var currMapper = new MapperWithObjectCreation<ICurrency, CurrencyInfo>();
            ratioInfo.Base = currMapper.CreateMappedObject(ratioInfo.Base);
            ratioInfo.Target = currMapper.CreateMappedObject(ratioInfo.Target);
            return ratioInfo;
        }

    }
}
