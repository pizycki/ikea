﻿using IKEA.CurrencyAPI.SOAP.Models;
using System.Collections.Generic;
using System.ServiceModel;

namespace IKEA.CurrencyAPI.SOAP.Contracts
{
    [ServiceContract(SessionMode = SessionMode.NotAllowed, Namespace = Metadata.ServicesNamespace)]
    public interface ICurrencyRateService
    {
        /// <summary>
        /// Pobiera aktualny kurs walut.
        /// </summary>
        /// <param name="baseCurrency">Waluta podstawowa.</param>
        /// <param name="targetCurrency">Waluta, w której zostanie przedstawiony wynik.</param>
        /// <returns>Kurs walut.</returns>
        [OperationContract]
        CurrencyRatioInfo GetCurrencyRatio(CurrencyInfo baseCurrency, CurrencyInfo targetCurrency);

        /// <summary>
        /// Pobiera wszystkie wspierane waluty.
        /// </summary>
        /// <returns>Kolekcja walut.</returns>
        [OperationContract]
        IEnumerable<CurrencyInfo> GetCurrencies();

        /// <summary>
        /// Pobiera słownik skrótowych nazw dla wspieranych walut.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        IDictionary<string, CurrencyInfo> GetCurrencyDictionary();

        /// <summary>
        /// Przelicza wartość z jednej waluty na drugą.
        /// </summary>
        /// <param name="baseCurrency">Waluta bazowa</param>
        /// <param name="baseCurrencyValue">Wartość bazowa</param>
        /// <param name="targetCurrency">Waluta docelowa</param>
        /// <returns>Wartość waluty docelowej.</returns>
        [OperationContract]
        float? ConvertCurrency(CurrencyInfo baseCurrency, float baseCurrencyValue, CurrencyInfo targetCurrency);
    }
}
