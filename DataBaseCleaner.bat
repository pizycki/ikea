SET CurrentPath=%CD%
SET ConfigFile=%CurrentPath%\IKEA.CurrencySrv.Console\App.config
SET MigrateExe=.\packages\EntityFramework.6.1.1\tools\migrate.exe

%MigrateExe% IKEA.DAL.EF.dll /StartUpDirectory:%CurrentPath%\IKEA.DAL.EF\bin\Debug /startUpConfigurationFile:"%ConfigFile%" /TargetMigration:0 /Force

%MigrateExe% IKEA.DAL.EF.dll /StartUpDirectory:%CurrentPath%\IKEA.DAL.EF\bin\Debug /startUpConfigurationFile:"%ConfigFile%" /Force