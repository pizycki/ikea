﻿using IKEA.Common.Contracts.POCO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace IKEA.FCCA.Client.Tests
{
    [TestClass]
    public class CurrencyDataProviderTests
    {
        [TestMethod]
        public void Should_return_currency_ratio()
        {
            var bc = new Mock<ICurrency>();
            bc.Setup(x => x.Abbreviation).Returns("PLN");

            var tc = new Mock<ICurrency>();
            tc.Setup(x => x.Abbreviation).Returns("GBP");

            var cr = new Mock<ICurrencyRatio>();
            cr.Setup(x => x.Base).Returns(bc.Object);
            cr.Setup(x => x.Target).Returns(tc.Object);

            var cdp = new CurrencyDataProvider();

            var result = cdp.GetCurrencyRatio(cr.Object);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
        }
    }
}
