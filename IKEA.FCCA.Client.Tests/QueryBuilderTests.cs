﻿using IKEA.Common.Contracts.DataProvider.Exceptions;
using IKEA.Common.Contracts.DataProvider.QueryBuilder;
using IKEA.Common.Contracts.POCO;
using Moq;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IKEA.FCCA.Client.Tests
{
    [TestClass]
    public class QueryBuilderTests
    {
        #region ==========  Helpers ==========

        protected ICurrency CreateMockedCurrency(string name, string abbr, string country)
        {
            var currencyMock = new Mock<ICurrency>();
            currencyMock.Setup(m => m.Name).Returns(name);
            currencyMock.Setup(m => m.Abbreviation).Returns(abbr);
            currencyMock.Setup(m => m.Country).Returns(country);
            return currencyMock.Object;
        }

        #endregion

        [TestMethod]
        public void Should_build_single_request_query_and_pass()
        {
            // Arrange 
            var baseCurrency = CreateMockedCurrency(null, "PLN", null);
            var targetCurrency = CreateMockedCurrency(null, "USD", null);

            // Act
            IQueryBuilder queryBuilder = new HttpQueryBuilder();
            var query = queryBuilder.Create<SingleCurrencyRatioQuery>().From(baseCurrency).To(targetCurrency).Query;

            // Assert
            const string expected = "/convert?q=PLN-USD";
            Assert.IsNotNull(query);
            Assert.AreNotEqual("", query);
            Assert.AreEqual(expected, query);
        }


        [TestMethod]
        public void Should_build_single_compact_request_query_and_pass()
        {
            // Arrange 
            var baseCurrency = CreateMockedCurrency(null, "PLN", null);
            var targetCurrency = CreateMockedCurrency(null, "USD", null);

            // Act
            IQueryBuilder queryBuilder = new HttpQueryBuilder();
            var query = queryBuilder.Create<SingleCurrencyRatioQuery>()
                .From(baseCurrency)
                .To(targetCurrency)
                .InCompactMode()
                .Query;

            // Assert
            const string expected = "/convert?q=PLN-USD&compact=y";
            Assert.IsNotNull(query);
            Assert.AreNotEqual("", query);
            Assert.AreEqual(expected, query);
        }

        [TestMethod]
        public void Should_fail_building_empty_single_query()
        {
            // Arrange

            // Act
            string query = null;
            Exception ex = null;
            try
            {
                IQueryBuilder queryBuilder = new HttpQueryBuilder();
                query = queryBuilder.Create<SingleCurrencyRatioQuery>().Query;
            }
            catch (IncompleteRequestException e)
            {
                ex = e;
            }

            // Assert
            Assert.IsNull(query);
            Assert.IsNotNull(ex);
            Assert.IsInstanceOfType(ex, typeof(IncompleteRequestException));
        }

        [TestMethod]
        public void Should_build_multi_request_query_and_pass()
        {
            // Arrange 

            // 1st ratio
            var baseCurrency1 = CreateMockedCurrency(null, "PLN", null);
            var targetCurrency1 = CreateMockedCurrency(null, "USD", null);

            // 2nd ratio
            var baseCurrency2 = CreateMockedCurrency(null, "EUR", null);
            var targetCurrency2 = CreateMockedCurrency(null, "GBP", null);

            // Act            
            IQueryBuilder queryBuilder = new HttpQueryBuilder();
            var query = queryBuilder.Create<MultiCurrencyRatioQuery>()
                .From(baseCurrency1).To(targetCurrency1)
                .And().From(baseCurrency2).To(targetCurrency2)
                .Query;

            // Assert
            const string expected = "/convert?q=PLN-USD,EUR-GBP";
            Assert.IsNotNull(query);
            Assert.AreNotEqual("", query);
            Assert.AreEqual(expected, query);

        }

        [TestMethod]
        public void Should_build_multi_request_compact_query_and_pass()
        {
            // Arrange 

            // 1st ratio
            var baseCurrency1 = CreateMockedCurrency(null, "PLN", null);
            var targetCurrency1 = CreateMockedCurrency(null, "USD", null);

            // 2nd ratio
            var baseCurrency2 = CreateMockedCurrency(null, "EUR", null);
            var targetCurrency2 = CreateMockedCurrency(null, "GBP", null);

            // Act            
            IQueryBuilder queryBuilder = new HttpQueryBuilder();
            var query = queryBuilder.Create<MultiCurrencyRatioQuery>()
                .From(baseCurrency1).To(targetCurrency1)
                .And().From(baseCurrency2).To(targetCurrency2)
                .InCompactMode()
                .Query;

            // Assert
            const string expected = "/convert?q=PLN-USD,EUR-GBP&compact=y";
            Assert.IsNotNull(query);
            Assert.AreNotEqual("", query);
            Assert.AreEqual(expected, query);
        }

        [TestMethod]
        public void Should_build_multi_request_query_with_mixed_methods_order_and_pass()
        {
            // Arrange 

            // 1st ratio
            var baseCurrency1 = CreateMockedCurrency(null, "PLN", null);
            var targetCurrency1 = CreateMockedCurrency(null, "USD", null);

            // 2nd ratio
            var baseCurrency2 = CreateMockedCurrency(null, "EUR", null);
            var targetCurrency2 = CreateMockedCurrency(null, "GBP", null);

            // Act            
            IQueryBuilder queryBuilder = new HttpQueryBuilder();
            var query = queryBuilder.Create<MultiCurrencyRatioQuery>()
                .To(targetCurrency1).From(baseCurrency1) // mixed methods order
                .And().From(baseCurrency2).To(targetCurrency2)
                .Query;

            // Assert
            const string expected = "/convert?q=PLN-USD,EUR-GBP";
            Assert.IsNotNull(query);
            Assert.AreNotEqual("", query);
            Assert.AreEqual(expected, query);
        }

        [TestMethod]
        public void Should_build_AllCurrencies_query_and_pass()
        {
            // Arrange

            // Act
            IQueryBuilder queryBuilder = new HttpQueryBuilder();
            var query = queryBuilder.Create<AllCurrenciesQuery>().Query;

            // Assert
            const string expected = "/currencies";
            Assert.AreEqual(expected, query);
        }

        [TestMethod]
        public void Should_build_multi_request_query_with_and_method_in_end_and_fail()
        {
            // Arrange

            // 1st ratio
            var baseCurrency1 = CreateMockedCurrency(null, "PLN", null);
            var targetCurrency1 = CreateMockedCurrency(null, "USD", null);

            // 2nd ratio
            var baseCurrency2 = CreateMockedCurrency(null, "EUR", null);
            var targetCurrency2 = CreateMockedCurrency(null, "GBP", null);

            // Act            
            IQueryBuilder queryBuilder = new HttpQueryBuilder();

            string query = null;
            Exception ex = null;
            try
            {
                query = queryBuilder.Create<MultiCurrencyRatioQuery>()
                .From(baseCurrency1).To(targetCurrency1)
                .And()
                .From(baseCurrency2).To(targetCurrency2)
                .And()
                .Query;
            }
            catch (IncompleteRequestException e)
            {
                ex = e;
            }

            // Assert
            Assert.IsNull(query);
            Assert.IsNotNull(ex);
            Assert.IsInstanceOfType(ex, typeof(IncompleteRequestException));
        }
    }
}
