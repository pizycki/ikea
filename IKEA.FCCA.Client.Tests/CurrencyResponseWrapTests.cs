﻿using IKEA.FCCA.Client.Currencies;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;
using IKEA.FCCA.Client.Contracts;

namespace IKEA.FCCA.Client.Tests
{
    [TestClass]
    public class CurrencyResponseWrapTests
    {   
        [TestMethod]
        public void Should_return_good_Name_and_pass()
        {
            // Arrange
            string name = "Some currency name";
            var currencyResponseMock = new Mock<ICurrencyResponse>();
            var currencyResponse = currencyResponseMock.Object;
            currencyResponseMock.Setup(x => x.CurrencyName).Returns(name);

            // Act
            var wrap = new CurrencyResponseWrap(currencyResponse);

            // Assert
            Assert.IsNotNull(wrap);
            Assert.AreEqual(wrap.Name, name);
        }

        [TestMethod]
        public void Should_return_good_abbreviation_and_pass()
        {
            // Arrange
            string abbr = "TST";
            var currencyResponseMock = new Mock<ICurrencyResponse>();
            var currencyResponse = currencyResponseMock.Object;
            currencyResponseMock.Setup(x => x.Id).Returns(abbr);

            // Act
            var wrap = new CurrencyResponseWrap(currencyResponse);

            // Assert
            Assert.IsNotNull(wrap);
            Assert.AreEqual(wrap.Abbreviation, abbr);
        }

        [TestMethod]
        public void Should_return_good_country_and_pass()
        {
            // Arrange
            string country = "Great Test of Country";
            var currencyResponseMock = new Mock<ICurrencyResponse>();
            var currencyResponse = currencyResponseMock.Object;
            currencyResponseMock.Setup(x => x.Country).Returns(country);

            // Act
            var wrap = new CurrencyResponseWrap(currencyResponse);

            // Assert
            Assert.IsNotNull(wrap);
            Assert.AreEqual(wrap.Country, country);
        }

        [TestMethod]
        public void Should_return_wrapped_object_instance_and_pass()
        {
            // Arrange
            var currencyResponseMock = new Mock<ICurrencyResponse>();
            var currencyResponse = currencyResponseMock.Object;

            // Act
            var wrap = new CurrencyResponseWrap(currencyResponse);

            // Assert
            Assert.IsNotNull(wrap);
            Assert.IsNotNull(wrap.CurrencyResponse);
            Assert.AreEqual(wrap.CurrencyResponse, currencyResponse);
        }
    }
}
