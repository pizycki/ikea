﻿using System.Collections.Generic;

namespace IKEA.FCCA.Client.Contracts
{
    public interface ICurrencyRatiosResponse
    {
        IDictionary<string, ICurrencyRatioResponse> Dictionary { get; set; }
    }
}
