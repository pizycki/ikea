﻿
namespace IKEA.FCCA.Client.Contracts
{
    public interface ICurrencyRatioResponse
    {
        string Id { get; set; }
        string Base { get; set; }
        string Target { get; set; }
        float Value { get; set; }
    }
}
