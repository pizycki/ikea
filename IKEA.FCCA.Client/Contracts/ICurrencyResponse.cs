﻿namespace IKEA.FCCA.Client.Contracts
{
    public interface ICurrencyResponse
    {
        string Id { get; set; }
        string CurrencyName { get; set; }
        string Country { get; set; }
    }
}
