﻿using IKEA.Common.Contracts.DataProvider;
using IKEA.Common.Contracts.DataProvider.Exceptions;
using IKEA.Common.Contracts.DataProvider.QueryBuilder;
using IKEA.Common.Contracts.POCO;
using IKEA.FCCA.Client.Currencies;
using IKEA.Common.Models.Domain;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Linq;

namespace IKEA.FCCA.Client
{
    /// <summary>
    /// Dostarcza dane o kursach walut.
    /// Klient dla darmowego serwisu FreeCurrencyConverterApi.
    /// </summary>
    /// <see cref="http://www.freecurrencyconverterapi.com"/>
    public class CurrencyDataProvider : ICurrencyDataProvider
    {
        private readonly string _serviceAddress;
        public const string DefaultServiceAddress = @"http://www.freecurrencyconverterapi.com/api";

        public CurrencyDataProvider(string serviceAddress = DefaultServiceAddress)
        {
            _serviceAddress = serviceAddress;
        }

        /// <summary>
        /// Pobiera zbiór wszystkich obsługiwanych walut przez serwis.
        /// </summary>
        /// <returns>Zbiór obsługiwanych walut.</returns>
        public IEnumerable<ICurrency> GetAvaibleCurrencies()
        {
            IQueryBuilder builder = new HttpQueryBuilder();
            var query = builder.Create<AllCurrenciesQuery>().Query;

            var httpClnt = new RestClient(_serviceAddress + query);
            IRestRequest request = new RestRequest();
            var response = httpClnt.Execute(request);
            var content = response.Content;
            var resultCurrencies = JsonConvert.DeserializeObject<AvaibleCurrencies>(content);
            var currenciesSet = new HashSet<ICurrency>();
            foreach (var c in resultCurrencies.Dictionary.Values)
            {
                var wrap = new CurrencyResponseWrap(c);
                currenciesSet.Add(wrap);
            }

            return currenciesSet;
        }

        /// <summary>
        /// Pobiera aktualny kurs walut.
        /// </summary>
        /// <param name="currencyRatio">Kurs walut do wypełnienia</param>
        /// <returns>Wypełniony kurs walut.</returns>
        public ICurrencyRatio GetCurrencyRatio(ICurrencyRatio currencyRatio)
        {
            return GetCurrencyRatio(currencyRatio.Base, currencyRatio.Target);
        }

        /// <summary>
        /// Pobiera aktualny kurs walut.
        /// </summary>
        /// <param name="baseCurrency">Waluta bazowa</param>
        /// <param name="targetCurrency">Waluta docelowa</param>
        /// <returns>Aktualny kurs walut</returns>
        public ICurrencyRatio GetCurrencyRatio(ICurrency baseCurrency, ICurrency targetCurrency)
        {
            ICurrencyRatio currencyRatio = new CurrencyRatio
            {
                Base = baseCurrency,
                Target = targetCurrency
            };

            GetCurrencyRatio(ref currencyRatio);

            return currencyRatio;
        }

        /// <summary>
        /// Pobiera aktualny kurs walut.
        /// </summary>
        /// <param name="currencyRatio">Kurs walut do wypełnienia</param>
        void ICurrencyDataProvider.GetCurrencyRatio(ref ICurrencyRatio currencyRatio)
        {
            GetCurrencyRatio(ref currencyRatio);
        }

        /// <summary>
        /// Pobiera aktualny kurs walut.
        /// </summary>
        /// <param name="currencyRatio">Kurs walut do wypełnienia</param>
        /// <param name="inCompactMode">Czy żądanie i odpowiedź ma być w krótszej formie.</param>
        private void GetCurrencyRatio(ref ICurrencyRatio currencyRatio, bool inCompactMode = false)
        {
            IQueryBuilder builder = new HttpQueryBuilder();
            var query = builder.Create<SingleCurrencyRatioQuery>().From(currencyRatio.Base).To(currencyRatio.Target).Query;

            var httpClnt = new RestClient(_serviceAddress);
            var request = new RestRequest(query);
            var response = httpClnt.Execute(request);
            var content = response.Content;

            var resultCurrencyRatios = JsonConvert.DeserializeObject<CurrencyRatiosResponse>(content);

            if (resultCurrencyRatios.Dictionary.Count != 1)
                throw new InvalidResponseException();

            currencyRatio.Value = resultCurrencyRatios.Dictionary.Single().Value.Value;
        }

        /// <summary>
        /// Pobiera aktualne kursy dla podanych walut.
        /// </summary>
        /// <param name="currencyRatios">Kursy walut do wypełnienia</param>
        /// <returns>Wypełnione kursy walut.</returns>
        ICurrencyRatio[] ICurrencyDataProvider.GetCurrencyRatios(ICurrencyRatio[] currencyRatios)
        {
            return GetCurrencyRatios(currencyRatios);
        }

        /// <summary>
        /// Pobiera aktualne kursy dla podanych walut.
        /// </summary>
        /// <param name="currencyRatios">Kursy walut do wypełnienia</param>
        /// <param name="inCompactMode">Czy rządanie i odpowiedź ma być w krótszej formie.</param>
        /// <returns>Wypełnione kursy walut.</returns>
        public ICurrencyRatio[] GetCurrencyRatios(ICurrencyRatio[] requestedCurrencyRatios, bool inCompactMode = false)
        {
            // Zbuduj zapytanie
            IQueryBuilder builder = new HttpQueryBuilder();
            var query = builder.Create<MultiCurrencyRatioQuery>();

            int lastIdx = requestedCurrencyRatios.Length - 1;
            for (int idx = 0; idx < requestedCurrencyRatios.Length; idx++)
            {
                var ratio = requestedCurrencyRatios[idx];
                query.From(ratio.Base).To(ratio.Target);

                if (idx != lastIdx)
                    query.And(); // kontynuuj dodawanie
            }

            if (inCompactMode)
                query.InCompactMode();

            string httpQuery = query.Query;

            // Wyślij zapytanie
            var httpClnt = new RestClient(_serviceAddress);
            var httpRequest = new RestRequest(httpQuery);
            var httpResponse = httpClnt.Execute(httpRequest);
            var content = httpResponse.Content;

            // Parsuj odpowiedź
            var currencyRatiosResponses = JsonConvert.DeserializeObject<CurrencyRatiosResponse>(content);

            if (currencyRatiosResponses.Dictionary.Count == 0)
                return null;

            foreach (var reqCurrRatio in requestedCurrencyRatios)
            {
                var response = currencyRatiosResponses.Dictionary.Values.First(x => x.Base == reqCurrRatio.Base.Abbreviation
                    && x.Target == reqCurrRatio.Target.Abbreviation);
                reqCurrRatio.Value = response.Value;
            }

            return requestedCurrencyRatios;
        }
    }
}
