﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace IKEA.FCCA.Client.Currencies
{
    /// <summary>
    /// Model dla odpowiedzi z serwisu FreeCurrencyConverterApi. 
    /// Zawiera listę wszystkich dostępnych walut.
    /// </summary>
    public sealed class AvaibleCurrencies
    {
        [JsonProperty("results")]
        public Dictionary<string, CurrencyResponse> Dictionary { get; set; }
    }
}
