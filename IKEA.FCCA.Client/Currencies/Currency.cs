﻿using IKEA.Common.Contracts.POCO;
using IKEA.FCCA.Client.Contracts;
using Newtonsoft.Json;

namespace IKEA.FCCA.Client.Currencies
{
    /// <summary>
    /// Wrapper dla CurrencyResponse.
    /// </summary>
    public class CurrencyResponseWrap : ICurrency
    {
        private readonly ICurrencyResponse _response;
        public CurrencyResponseWrap(ICurrencyResponse currencyResponse)
        {
            _response = currencyResponse;
        }

        public string Name
        {
            get { return _response.CurrencyName; }
            set { _response.CurrencyName = value; }
        }

        public string Abbreviation
        {
            get { return _response.Id; }
            set { _response.Id = value; }
        }

        public string Country
        {
            get { return _response.Country; }
            set { _response.Country = value; }
        }

        public ICurrencyResponse CurrencyResponse { get { return _response; } }

        public string Key
        {
            get { return Abbreviation; }
        }
    }

    /// <summary>
    /// Model dla odpowiedzi z serwisu FreeCurrencyConverterApi. 
    /// Zawiera informacje o walucie.
    /// </summary>
    public sealed class CurrencyResponse : ICurrencyResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("currencyName")]
        public string CurrencyName { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }
    }
}