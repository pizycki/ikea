﻿using IKEA.Common.Contracts.POCO;
using IKEA.Common.Exceptions;
using IKEA.FCCA.Client.Contracts;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace IKEA.FCCA.Client.Currencies
{
    /// <summary>
    /// Wrapper dla SingleCurrencyRatioResponse. Zawiera dodatkową logike.
    /// </summary>
    public class CurrencyRatioResponseWrap : ICurrencyRatio
    {
        ICurrencyRatioResponse _response;
        ICurrency _baseCurrency;
        ICurrency _targetCurrency;
        DateTime? _creationTime;

        public CurrencyRatioResponseWrap(ICurrencyRatioResponse response, ICurrency baseCurrency, ICurrency targetCurrency, DateTime? creationTime)
        {
            _response = response;
            _baseCurrency = baseCurrency;
            _targetCurrency = targetCurrency;
            _creationTime = creationTime ?? DateTime.Now;

            if (!DoAbbreviationsMatch())
                throw new ServerException("Abbreviations don't match.");
        }

        protected virtual bool DoAbbreviationsMatch()
        {
            return _response.Base == _baseCurrency.Abbreviation
                && _response.Target == _targetCurrency.Abbreviation;
        }

        public ICurrency Base
        {
            get { return _baseCurrency; }
            set
            {
                _baseCurrency = value;
                DoAbbreviationsMatch();
            }
        }

        public ICurrency Target
        {
            get { return _targetCurrency; }
            set
            {
                _targetCurrency = value;
                DoAbbreviationsMatch();
            }
        }

        public float? Value
        {
            get { return _response.Value; }
            set { _response.Value = value.Value; }
        }

        public DateTime? CreationTime
        {
            get { return _creationTime; }
            set { _creationTime = value; }
        }
    }
    /// <summary>
    /// Model dla odpowiedzi z serwisu FreeCurrencyConverterApi. 
    /// Zawiera listę kursów walut.
    /// </summary>
    public class CurrencyRatiosResponse
    {
        /// <summary>
        /// Słownik z kursami walut.
        /// </summary>
        /// <example>{"query":{"count":1},"results":{"USD-PHP":{"fr":"USD","id":"USD-PHP","to":"PHP","val":43.47}}}</example>
        [JsonProperty("results")]
        public IDictionary<string, SingleCurrencyRatioResponse> Dictionary { get; set; }
    }

    /// <summary>
    /// Model dla odpowiedzi z serwisu FreeCurrencyConverterApi. 
    /// Zawiera informację o kursie walut.
    /// </summary>
    public sealed class SingleCurrencyRatioResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("fr")]
        public string Base { get; set; }

        [JsonProperty("to")]
        public string Target { get; set; }

        [JsonProperty("val")]
        public float Value { get; set; }
    }
}