﻿using IKEA.

namespace IKEA.FCCA.Client.Currencies
{
    public class RequestedCurrencyPair : IRequestedCurrencyPair
    {
        public ICurrency Base { get; set; }
        public ICurrency Target { get; set; }
    }
}
