﻿using System.Linq;
using IKEA.Common.Contracts.DataProvider.Exceptions;
using IKEA.Common.Contracts.DataProvider.QueryBuilder;
using IKEA.Common.Contracts.POCO;
using System;
using System.Collections.Generic;
using System.Text;

namespace IKEA.FCCA.Client
{
    /// <summary>
    /// Klasa do budowania zapytań HTTP do serwisu FreeCurrencyConverterAPI.
    /// </summary>
    public class HttpQueryBuilder : IQueryBuilder
    {
        /// <summary>
        /// Obsługiwane typy zapytań.
        /// </summary>
        public readonly Type[] SupportedTypes = { 
            typeof(SingleCurrencyRatioQuery),
            typeof(AllCurrenciesQuery),
            typeof(MultiCurrencyRatioQuery)
        };

        /// <summary>
        /// Tworzy nową instancje zapytania o określonym typie.
        /// </summary>
        /// <typeparam name="T">Typ zapytania.</typeparam>
        /// <returns>Zapytanie do konfiguracji.</returns>
        /// <example>
        /// new HttpQueryBuilder().Create<MultiCurrencyRatioQuery>().From(baseCurrency).To(targetCurrency).InCompactMode().Query;
        /// </example>
        public T Create<T>() where T : class, IQuery
        {
            if (!SupportedTypes.Any(t => typeof(T).IsEquivalentTo(t)))
                throw new UnsupportedQueryTypeException { QueryType = typeof(T) };

            var query = Activator.CreateInstance<T>();
            return query;
        }
    }

    /// <summary>
    /// Zapytanie dla pojedyńczej pary walut. 
    /// </summary>
    public class SingleCurrencyRatioQuery : IQuery
    {
        private ICurrency _baseCurrency;
        private ICurrency _targetCurrency;
        private bool _compactResponse;

        /// <summary>
        /// Określa bazową walutę.
        /// </summary>
        /// <param name="baseCurrency">Bazowa waluta.</param>
        /// <returns>Zaktualizowane zapytanie.</returns>
        public SingleCurrencyRatioQuery From(ICurrency baseCurrency)
        {
            _baseCurrency = baseCurrency;
            return this;
        }

        /// <summary>
        /// Określa docelową walutę.
        /// </summary>
        /// <param name="targetCurrency">Docelowa waluta.</param>
        /// <returns>Zaktualizowane zapytanie.</returns>
        public SingleCurrencyRatioQuery To(ICurrency targetCurrency)
        {
            _targetCurrency = targetCurrency;
            return this;
        }

        /// <summary>
        /// Generuje zapytanie HTTP na podstawie podanych danych.
        /// </summary>
        /// <returns>Zapytanie HTTP.</returns>
        private string GenerateQuery()
        {
            if (_baseCurrency != null && _targetCurrency != null)
            {
                return string.Format("/convert?q={0}-{1}{2}"
                       , _baseCurrency.Abbreviation
                       , _targetCurrency.Abbreviation
                       , _compactResponse ? "&compact=y" : string.Empty);
            }

            throw new IncompleteRequestException();
        }

        /// <summary>
        /// Zapytanie HTTP
        /// </summary>
        public string Query
        {
            get { return GenerateQuery(); }
        }

        /// <summary>
        /// Czy odpowiedź ma być w krótszej formie.
        /// </summary>
        public bool IsInCompactMode
        {
            get { return _compactResponse; }
        }

        /// <summary>
        /// Ustawia zapytanie idx odpowiedź w formie skróconej.
        /// </summary>
        /// <returns>Zaktualizowane zapytanie.</returns>
        public SingleCurrencyRatioQuery InCompactMode()
        {
            _compactResponse = true;
            return this;
        }
    }

    /// <summary>
    /// Zapytanie dla wielu par walut.
    /// Waluty powinny być dodawane parami (w dowolnej kolejności) idx oddzielane metodą And().
    /// </summary>
    public class MultiCurrencyRatioQuery : IQuery
    {
        private readonly ICollection<CurrencyRatioRequest> _requests = new List<CurrencyRatioRequest>();
        private readonly CurrencyRatioRequest _requestInBuild;
        private bool _inCompactMode;

        public MultiCurrencyRatioQuery()
        {
            _requestInBuild = new CurrencyRatioRequest();
        }

        /// <summary>
        /// Ustawia walute bazową na budowanej parze.
        /// </summary>
        /// <param name="baseCurrency">Waluta bazowa.</param>
        /// <returns>Zaktualizowane zapytanie.</returns>
        public MultiCurrencyRatioQuery From(ICurrency baseCurrency)
        {
            if (_requestInBuild != null)
            {
                _requestInBuild.BaseCurrency = baseCurrency;
            }
            return this;
        }

        /// <summary>
        /// Ustawia walute docelową na budowanej parze.
        /// </summary>
        /// <param name="targetCurrency">Docelowa waluta.</param>
        /// <returns>Zaktualizowane zapytanie.</returns>
        public MultiCurrencyRatioQuery To(ICurrency targetCurrency)
        {
            if (_requestInBuild != null)
            {
                _requestInBuild.TargetCurrency = targetCurrency;
            }
            return this;
        }

        /// <summary>
        /// Oddziela pary waluty. Dodaje aktualnie budowaną walutę do kolekcji walut idx przygotowuje miejsce dla nowej pary.
        /// Jeżeli para jest niekompletna, rzuca wyjątek.
        /// </summary>
        /// <returns>Zaktualizowane zapytanie.</returns>
        public MultiCurrencyRatioQuery And()
        {
            AddBuildedRequest();
            return this;
        }

        /// <summary>
        /// Dodaje aktualnie budowaną parę walut do kolekcji par walut idx przygotowuje miejsce dla nowej pary.
        /// </summary>
        private void AddBuildedRequest()
        {
            if (!IsBuildedRequestReady())
                throw new IncompleteRequestException();

            if (_requests == null)
                throw new NullRequestsListException();

            _requests.Add((CurrencyRatioRequest)_requestInBuild.Clone());

            _requestInBuild.Clear();
        }

        /// <summary>
        /// Sprawdza czy budowana para walut jest już gotowa do dodania. 
        /// </summary>
        /// <returns></returns>
        private bool IsBuildedRequestReady()
        {
            if (_requestInBuild != null)
                if (_requestInBuild.BaseCurrency != null && _requestInBuild.TargetCurrency != null)
                    return true;

            return false;
        }

        /// <summary>
        /// Generuje zapytanie HTTP.
        /// </summary>
        /// <returns>Zapytanie HTTP</returns>
        private string GenerateQuery()
        {
            if (_requests == null)
                return string.Empty;

            if (_requestInBuild != null)
                AddBuildedRequest();

            if (_requests.Count == 0)
                return string.Empty;

            var sb = new StringBuilder();

            sb.Append("/convert?q=");
            foreach (var request in _requests)
            {
                sb.Append(request.BaseCurrency.Abbreviation);
                sb.Append('-');
                sb.Append(request.TargetCurrency.Abbreviation);
                sb.Append(',');
            }

            // Delete last comma
            sb.Remove(sb.Length - 1, 1);

            if (_inCompactMode)
            {
                sb.Append("&compact=y");
            }

            return sb.ToString();
        }

        /// <summary>
        /// Zapytanie HTTP
        /// </summary>
        public string Query
        {
            get { return GenerateQuery(); }
        }

        /// <summary>
        /// Ustawia zapytanie idx odpowiedź w tryb skrócony.
        /// </summary>
        /// <returns>Zaktualizowane zapytanie.</returns>
        public MultiCurrencyRatioQuery InCompactMode()
        {
            _inCompactMode = true;
            return this;
        }

        /// <summary>
        /// Para walut dla których ma być sprawdzony kurs.
        /// </summary>
        class CurrencyRatioRequest : ICloneable
        {
            /// <summary>
            /// Waluta bazowa
            /// </summary>
            public ICurrency BaseCurrency { get; set; }

            /// <summary>
            /// Waluta docelowa
            /// </summary>
            public ICurrency TargetCurrency { get; set; }

            /// <summary>
            /// Tworzy płytką kopie.
            /// </summary>
            /// <returns></returns>
            public object Clone()
            {
                var clone = new CurrencyRatioRequest
                {
                    BaseCurrency = BaseCurrency,
                    TargetCurrency = TargetCurrency
                };
                return clone;
            }

            /// <summary>
            /// Czyści pola pary.
            /// </summary>
            public void Clear()
            {
                BaseCurrency = null;
                TargetCurrency = null;
            }
        }
    }

    /// <summary>
    /// Zapytanie do pobrania wszystkich dostępnych walut z serwisu FreeCurrencyConverterAPI
    /// </summary>
    public class AllCurrenciesQuery : IQuery
    {
        /// <summary>
        /// Zapytanie HTTP
        /// </summary>
        public string Query
        {
            get { return "/currencies"; }
        }
    }
}