﻿using System.Collections.Generic;
using System.Linq;
using IKEA.Common.Contracts.DAL;
using IKEA.Common.Contracts.POCO;
using IKEA.Common.Utils;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Currency = IKEA.Common.Models.DAL.Mongo.Currency;

namespace IKEA.DAL.Mongo
{
    public class CurrencyDal : ICurrencyRepository
    {
        private MongoDatabase _db;
        private readonly string _connectionString;
        private readonly string _databaseName;
        private readonly IMapperWithObjectCreation<ICurrency, Currency> _mapper;

        public CurrencyDal(IMapperWithObjectCreation<ICurrency, Currency> mapper)
        {
            _connectionString = ConfigGetter.GetValueFor(Constants.MongoConnectionStringConfigKey);
            _databaseName = ConfigGetter.GetValueFor(Constants.MongoDatabaseNameConfigKey);
            _mapper = mapper;

            OpenConnection();
        }

        public void OpenConnection()
        {
            _db = MongoHelper.GetDatabase(_databaseName);
        }

        private MongoCollection<Currency> GetCurrencyCollection()
        {
            return _db.GetCollection<Currency>(Constants.MongoTableName);
        }

        public ICurrency GetByAbbreviation(string abbreviation)
        {
            var collection = GetCurrencyCollection();
            var fieldName = ReflectionUtils.GetPropertyName(() => new Currency().Abbreviation);
            var query = new QueryDocument(fieldName, abbreviation);
            var foundCurrencies = collection.Find(query);
            return foundCurrencies.FirstOrDefault();
        }

        public IEnumerable<ICurrency> GetManyByAbbreviation(IEnumerable<string> abbreviations)
        {
            var collection = GetCurrencyCollection();
            var fieldName = ReflectionUtils.GetPropertyName(() => new Currency().Abbreviation);
            var currencies = abbreviations.Select(item => Query.EQ(fieldName, item))
                .Select(query => collection
                    .Find(query)
                    .FirstOrDefault())
                .ToList();

            return currencies;
        }

        public IEnumerable<ICurrency> GetAll()
        {
            return GetCurrencyCollection().FindAll();
        }

        public ICurrency GetSingle(int id)
        {
            var collection = GetCurrencyCollection();
            var fieldName = ReflectionUtils.GetPropertyName(() => new Currency().Abbreviation);
            var query = new QueryDocument(fieldName, id);
            var foundCurrencies = collection.Find(query);
            return foundCurrencies.FirstOrDefault();
        }

        public void Add(params ICurrency[] items)
        {
            var collection = GetCurrencyCollection();
            var mappedItemsCollection = items.Select(x => _mapper.CreateMappedObject(x)).ToList();
            collection.InsertBatch(mappedItemsCollection);
        }

        public class Constants
        {
            public const string MongoTableName = "Currencies";
            public const string MongoConnectionStringConfigKey = "MongoConnectionString";
            public const string MongoDatabaseNameConfigKey = "MongoDatabaseName";
        }
    }
}
