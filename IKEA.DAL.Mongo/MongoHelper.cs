﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IKEA.DAL.Mongo
{
    public class MongoHelper
    {
        public static MongoDatabase GetDatabase(string databaseName, string connectionString = null)
        {
            var client = connectionString == null
                ? new MongoClient() // localhost
                : new MongoClient(connectionString);
            var srv = client.GetServer();
            var db = srv.GetDatabase(databaseName);
            return db;
        }
    }
}
