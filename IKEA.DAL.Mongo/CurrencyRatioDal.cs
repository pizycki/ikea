﻿using IKEA.Common.Contracts.DAL;
using IKEA.Common.Contracts.POCO;
using IKEA.Common.Utils;
using MongoDB.Driver;
using System.Linq;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using CurrencyRatio = IKEA.Common.Models.DAL.Mongo.CurrencyRatio;

namespace IKEA.DAL.Mongo
{
    public class CurrencyRatioDal : ICurrencyRatioRepository
    {
        private MongoDatabase _db;
        private readonly IMapperWithObjectCreation<ICurrencyRatio, CurrencyRatio> _mapper;
        private readonly string _connectionString;
        private readonly string _databaseName;

        public CurrencyRatioDal(IMapperWithObjectCreation<ICurrencyRatio, CurrencyRatio> mapper)
        {
            _mapper = mapper;
            _connectionString = ConfigGetter.GetValueFor(Constants.MongoConnectionStringConfigKey);
            _databaseName = ConfigGetter.GetValueFor(Constants.MongoDatabaseNameConfigKey);

            OpenConnection();
        }

        public void OpenConnection()
        {
            _db = MongoHelper.GetDatabase(_databaseName);
        }

        private MongoCollection<CurrencyRatio> GetCurrencyRatiosCollection()
        {
            return _db.GetCollection<CurrencyRatio>(Constants.MongoTableName);
        }

        public ICurrencyRatio GetSingle(string ratioBaseAbbreviation, string ratioTargetAbbreviation)
        {
            var collection = GetCurrencyRatiosCollection();

            var query = Query.And(
                Query.EQ("Base.Abbreviation", ratioBaseAbbreviation),
                Query.EQ("Target.Abbreviation", ratioTargetAbbreviation)
                );
            var currencyRatios = collection.Find(query);
            var currencyRatio = currencyRatios.FirstOrDefault();
            return currencyRatio;
        }

        public void Add(params ICurrencyRatio[] currencyRatios)
        {
            var collection = GetCurrencyRatiosCollection();
            var mappedCurrencyRatios = currencyRatios.Select(x => _mapper.CreateMappedObject(x)).ToList();
            collection.InsertBatch(mappedCurrencyRatios);
        }

        public class Constants
        {
            public const string MongoTableName = "CurrencyRatios";
            public const string MongoConnectionStringConfigKey = "MongoConnectionString";
            public const string MongoDatabaseNameConfigKey = "MongoDatabaseName";
        }
    }
}
