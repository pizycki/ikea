﻿using System;
using System.Linq;
using IKEA.Common.Utils;

namespace IKEA.CurrencySrv.Console.Infrastructure
{
    public static class Config
    {
        public static string SoapWebServiceAddress
        {
            get { return ConfigGetter.GetValueFor(ConfigKeys.SoapWebServiceAddress); }
        }

        public static string WebApiWebServiceAddress
        {
            get { return ConfigGetter.GetValueFor(ConfigKeys.WebApiWebServiceAddress); }
        }

        public static string NancyFxWebServiceAddress
        {
            get { return ConfigGetter.GetValueFor(ConfigKeys.NancyFxWebServiceAddress); }
        }

        public static int DaysToUpdateCurrencyList
        {
            get
            {
                var rawValue = ConfigGetter.GetValueFor(ConfigKeys.DaysToUpdateCurrencyList);
                var value = Int32.Parse(rawValue);

                return value;
            }
        }

        public static int MinutesToUpdateRatio
        {
            get
            {
                var rawValue = ConfigGetter.GetValueFor(ConfigKeys.MinutesToUpdateRatio);
                var value = Int32.Parse(rawValue);

                return value;
            }
        }

        public static string[] PopularCurrencies
        {
            get
            {
                var rawValue = ConfigGetter.GetValueFor(ConfigKeys.PopularCurrencies);
                string[] currencies = null;
                if (rawValue != null)
                    currencies = rawValue.Split(',').ToArray();
                return currencies;
            }
        }

        public static bool UseMemoryCachedCurrencyService
        {
            get
            {
                var value = ConfigGetter.GetValueFor(ConfigKeys.UseMemoryCachedCurrencyService);
                return bool.Parse(value);
            }
        }

        static class ConfigKeys
        {
            public const string SoapWebServiceAddress = "SoapWebServiceAddress";
            public const string RestWebServiceAdress = "RestWebServiceAddress";
            public const string DaysToUpdateCurrencyList = "DaysToUpdateCurrencyList";
            public const string PopularCurrencies = "PopularCurrencies";
            public const string UseMemoryCachedCurrencyService = "UseMemoryCachedCurrencyService";
            public const string MinutesToUpdateRatio = "MinutesToUpdateRatio";
            public const string WebApiWebServiceAddress = "WebApiWebServiceAddress";
            public const string NancyFxWebServiceAddress = "NancyFxWebServiceAddress";

        }
    }
}
