﻿using Autofac;
using System;
using IKEA.Application;
using IKEA.Common.Contracts.DAL;
using IKEA.Common.Contracts.DataProvider;
using IKEA.Common.Contracts.WebServices;
using IKEA.CurrencySrv.Console.Infrastructure;
using IKEA.CurrencySrv.Console.IoC;
using IKEA.CurrencySrv.Console.WebServiceModules;
using NLog;

namespace IKEA.CurrencySrv.Console
{
    sealed class Program
    {
        private Logger Log
        {
            get { return _logger; }
        }

        private ISoapWebService _soapWsModule;
        private DatabaseUpdaterAsync _databaseUpdater;
        private IWebApiWebServiceModule _webApiWebModule;
        private INancyWebServiceModule _nancyModule;
        private Logger _logger;

        private static void Main(string[] args)
        {
            var program = new Program();

            program.SetUpAll();
            program.StartAll();
        }

        /// <summary>
        /// Prepare all services for start up.
        /// </summary>
        private void SetUpAll()
        {
            _logger = LogManager.GetCurrentClassLogger();

            Log.Info("Setting up...");

            SetupSoapWsModule();
            SetupDataBaseUpdateModule();
            SetupWebApiModule();
            SetupNancyModule();

            Log.Info("All modules are set up!");
        }

        private void SetupSoapWsModule()
        {
            _soapWsModule = CreateSoapWebServiceModule();
            _soapWsModule.Setup();
            Log.Info("SOAP WebService is set up and ready for start.");
        }

        private void SetupWebApiModule()
        {
            _webApiWebModule = CreateWebApiWebServiceModule();
            _webApiWebModule.Setup();
            Log.Info("WebApi REST WebService is set up and ready for start.");
        }

        private void SetupNancyModule()
        {
            _nancyModule = CreateNancyWebServiceModule();
            _nancyModule.Setup();
            Log.Info("SOAP WebService is set up and ready for start.");
        }

        private void SetupDataBaseUpdateModule()
        {
            _databaseUpdater = CreateDatabaseUpdater();
            Log.Info("DatabaseUpdater is set up and ready for start.");
        }


        private void StartAll()
        {
            _soapWsModule.Start();
            Log.Info("SOAP WebService has started!");

            _databaseUpdater.Start();
            Log.Info("DatabaseUpdater has started!");

            _webApiWebModule.Start();
            Log.Info("WebApi WebService has started!");

            _nancyModule.Start();
            Log.Info("Nancy WebService has started!");

            System.Console.WriteLine("Any key pressed will close application.");
            System.Console.ReadLine();

            Log.Info("Closing web services...");
            StopAll();
            Log.Info("Web service has been closed successfuly.");

            Environment.Exit(0);
        }

        private void StopAll()
        {
            _soapWsModule.Stop();
            _databaseUpdater.Stop();
            _webApiWebModule.Stop();
            _nancyModule.Stop();
        }

        private static ISoapWebService CreateSoapWebServiceModule()
        {
            return DI.Container.Resolve<ISoapWebService>();
        }

        private static IWebApiWebServiceModule CreateWebApiWebServiceModule()
        {
            return DI.Container.Resolve<IWebApiWebServiceModule>();
        }

        private static INancyWebServiceModule CreateNancyWebServiceModule()
        {
            return DI.Container.Resolve<INancyWebServiceModule>();
        }

        private static DatabaseUpdaterAsync CreateDatabaseUpdater()
        {
            var asyncUpdater = new DatabaseUpdaterAsync(
                DI.Container.Resolve<ICurrencyDataProvider>(),
                DI.Container.Resolve<ICurrencyRepository>(),
                DI.Container.Resolve<ICurrencyRatioRepository>(),
                Config.DaysToUpdateCurrencyList,
                Config.MinutesToUpdateRatio,
                Config.PopularCurrencies);
            return asyncUpdater;
        }
    }
}
