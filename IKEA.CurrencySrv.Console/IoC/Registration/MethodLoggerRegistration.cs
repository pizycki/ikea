﻿using Autofac;
using IKEA.Common.Contracts.Application;

namespace IKEA.CurrencySrv.Console.IoC.Registration
{
    public class MethodLoggerRegistration<TMethodLogger> : ITypeRegistration
        where TMethodLogger : IMethodLogger
    {
        public void Register(ContainerBuilder builder)
        {
            builder.RegisterType<TMethodLogger>().As<IMethodLogger>();
        }
    }
}
