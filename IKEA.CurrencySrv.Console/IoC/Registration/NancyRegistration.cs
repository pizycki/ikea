﻿using Autofac;
using IKEA.Common.Contracts.WebServices;
using IKEA.CurrencySrv.Console.Infrastructure;

namespace IKEA.CurrencySrv.Console.IoC.Registration
{
    public class NancyRegistration<TNancyModule> : ITypeRegistration
        where TNancyModule : INancyWebServiceModule
    {
        public void Register(ContainerBuilder builder)
        {
            builder.RegisterType<TNancyModule>()
                .WithParameter(new NamedParameter("address", Config.NancyFxWebServiceAddress))
                .As<INancyWebServiceModule>();
        }
    }
}
