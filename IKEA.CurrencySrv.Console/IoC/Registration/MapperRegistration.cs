﻿using Autofac;
using IKEA.Common.Contracts.DAL;
using IKEA.Common.Contracts.POCO;
using System;
using IKEA.Common.Utils;

namespace IKEA.CurrencySrv.Console.IoC.Registration
{
    public class MapperRegistration<TCurrency, TCurrencyRatio> : ITypeRegistration
        where TCurrency : ICurrency, new()
        where TCurrencyRatio : ICurrencyRatio, new()
    {
        public void Register(ContainerBuilder builder)
        {
            // Currency
            builder.RegisterType<MapperWithObjectCreation<ICurrency, TCurrency>>().As<IMapperWithObjectCreation<ICurrency, TCurrency>>();

            // CurrencyRatio
            builder.RegisterInstance(GetCurrencyRatioMapper()).As<IMapperWithObjectCreation<ICurrencyRatio, TCurrencyRatio>>();
        }

        private static IMapperWithObjectCreation<ICurrencyRatio, TCurrencyRatio> GetCurrencyRatioMapper()
        {
            Func<ICurrency, TCurrency> mappingCurrency = (interfacedCurrency) =>
            {
                ICurrency mapped;
                if (interfacedCurrency is TCurrency)
                {
                    mapped = interfacedCurrency; // no mapping required
                }
                else
                {
                    var currecnyRepo = DI.Container.Resolve<ICurrencyRepository>();
                    mapped = currecnyRepo.GetByAbbreviation(interfacedCurrency.Abbreviation);
                }
                return (TCurrency)mapped;
            };

            var ratioMapper = new MapperWithObjectCreation<ICurrencyRatio, TCurrencyRatio>();
            ratioMapper.AddMapping((x, y) =>
            {
                y.Base = mappingCurrency(x.Base);
                y.Target = mappingCurrency(x.Target);
            });

            return ratioMapper;
        }
    }
}
