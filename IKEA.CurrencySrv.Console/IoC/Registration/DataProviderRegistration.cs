﻿using Autofac;
using IKEA.Common.Contracts.DataProvider;

namespace IKEA.CurrencySrv.Console.IoC.Registration
{
    public class DataProviderRegistration<TCurrencyDataProvider> : ITypeRegistration
        where TCurrencyDataProvider : ICurrencyDataProvider 
    {
        public void Register(ContainerBuilder builder)
        {
            builder.RegisterType<TCurrencyDataProvider>().As<ICurrencyDataProvider>();
        }
    }
}
