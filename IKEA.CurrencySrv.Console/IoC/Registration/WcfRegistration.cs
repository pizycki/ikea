﻿using Autofac;
using IKEA.CurrencyAPI.SOAP.Contracts;

namespace IKEA.CurrencySrv.Console.IoC.Registration
{
    public class WcfRegistration<TCurrencyRateService> : ITypeRegistration
        where TCurrencyRateService : ICurrencyRateService
    {
        public void Register(ContainerBuilder builder)
        {
            builder.RegisterType<TCurrencyRateService>().As<ICurrencyRateService>();
        }
    }
}
