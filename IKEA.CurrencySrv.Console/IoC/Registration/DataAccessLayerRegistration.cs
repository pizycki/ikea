﻿using Autofac;
using AutofacContrib.DynamicProxy2;
using IKEA.AOP;
using IKEA.Common.Contracts.DAL;

namespace IKEA.CurrencySrv.Console.IoC.Registration
{
    public class DataAccessLayerRegistration<TCurrencyRatioDal, TCurrencyDal> : ITypeRegistration
        where TCurrencyRatioDal : ICurrencyRatioRepository
        where TCurrencyDal : ICurrencyRepository
    {
        public void Register(ContainerBuilder builder)
        {
            builder.RegisterType<TCurrencyRatioDal>().As<ICurrencyRatioRepository>();

            builder.RegisterType<TCurrencyDal>().As<ICurrencyRepository>()
                .EnableClassInterceptors()
                .InterceptedBy(typeof(MethodCacheInterceptor));

        }
    }
}
