﻿using Autofac;
using Autofac.Integration.WebApi;
using IKEA.Common.Contracts.WebServices;
using IKEA.CurrencySrv.Console.Infrastructure;

namespace IKEA.CurrencySrv.Console.IoC.Registration
{
    public class WebApiRegistration<TWebApiModule> : ITypeRegistration
        where TWebApiModule : IWebApiWebServiceModule
    {
        public void Register(ContainerBuilder builder)
        {
            builder.RegisterType<TWebApiModule>()
                .WithParameter(new NamedParameter("address", Config.WebApiWebServiceAddress))
                .As<IWebApiWebServiceModule>();
            builder.RegisterApiControllers(typeof(CurrencyAPI.REST.WebApi2.CurrencyController).Assembly);
        }
    }
}