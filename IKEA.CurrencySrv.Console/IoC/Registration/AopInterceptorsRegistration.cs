﻿using Autofac;
using Castle.DynamicProxy;
using IKEA.Common.Contracts.Application;

namespace IKEA.CurrencySrv.Console.IoC.Registration
{
    public class AopInterceptorsRegistration<TMethodLogger, TMethodCacheInterceptor> : ITypeRegistration
        where TMethodLogger : IInterceptor, IMethodLogger
        where TMethodCacheInterceptor : IInterceptor
    {
        public void Register(ContainerBuilder builder)
        {
            builder.RegisterType<TMethodLogger>();
            builder.RegisterType<TMethodCacheInterceptor>();
        }
    }
}
