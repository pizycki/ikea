﻿using Autofac;
using IKEA.Common.Contracts.Application.Cache;

namespace IKEA.CurrencySrv.Console.IoC.Registration
{
    public class CacheRegistration<TCache, TCacheKeyGenerator> : ITypeRegistration
        where TCache : ICache
        where TCacheKeyGenerator : IMethodCacheKeyGenerator
    {
        public void Register(ContainerBuilder builder)
        {
            builder.RegisterType<TCache>().As<ICache>();
            builder.RegisterType<TCacheKeyGenerator>().As<IMethodCacheKeyGenerator>();
        }
    }
}
