﻿using Autofac;
using IKEA.Common.Contracts.WebServices;
using IKEA.CurrencySrv.Console.Infrastructure;

namespace IKEA.CurrencySrv.Console.IoC.Registration
{
    public class SoapWsRegistration<TSoapWs> : ITypeRegistration
        where TSoapWs : ISoapWebService
    {
        public void Register(ContainerBuilder builder)
        {
            builder.RegisterType<TSoapWs>()
                .WithParameter(new NamedParameter("address", Config.SoapWebServiceAddress))
                .As<ISoapWebService>();
        }
    }
}
