﻿using Autofac;
using Autofac.Builder;
using Castle.DynamicProxy;
using AutofacContrib.DynamicProxy2;

using IKEA.Application.Validators;
using IKEA.Common.Contracts.Application;
using IKEA.Common.Contracts.Application.Validators;
using IKEA.Common.Contracts.API;

namespace IKEA.CurrencySrv.Console.IoC.Registration
{
    /// <summary>
    /// Currency Server Registration
    /// </summary>
    public class CurrencyServerRegistration<TCurrencyServer, TCurrencyRatioValidatorFactory, TCurrencyValidatorFactory, TCurrencyRateService, TMethodLogger> : ITypeRegistration
        where TCurrencyServer : ICurrencyServer
        where TCurrencyRatioValidatorFactory : ICurrencyRatioValidatorFactory
        where TCurrencyValidatorFactory : ICurrencyValidatorFactory
        where TCurrencyRateService : ICurrencyRateService
        where TMethodLogger : IInterceptor
    {
        public virtual void Register(ContainerBuilder builder)
        {
            builder.RegisterType<TCurrencyServer>().As<ICurrencyServer>();
            builder.RegisterType<TCurrencyRatioValidatorFactory>().As<ICurrencyRatioValidatorFactory>();
            builder.RegisterType<TCurrencyValidatorFactory>().As<ICurrencyValidatorFactory>();

            RegisterCurrencyRateService(builder);
        }

        public virtual void RegisterCurrencyRateService(ContainerBuilder builder)
        {
            builder.RegisterType<TCurrencyServer>()
                .As<ICurrencyRateService>()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof(TMethodLogger));
        }
    }

    /// <summary>
    /// Memory cached Currency Server Registration.
    /// </summary>
    public class MemoryCachedCurrencyServerRegistration<TCurrencyServer, TCurrencyRatioValidatorFactory,
        TCurrencyValidatorFactory, TCurrencyRateService, TMethodLogger, TMethodCacheInterceptor>
        : CurrencyServerRegistration<TCurrencyServer, TCurrencyRatioValidatorFactory, TCurrencyValidatorFactory, TCurrencyRateService, TMethodLogger>
        where TCurrencyServer : ICurrencyServer
        where TCurrencyRatioValidatorFactory : ICurrencyRatioValidatorFactory
        where TCurrencyValidatorFactory : ICurrencyValidatorFactory
        where TCurrencyRateService : ICurrencyRateService
        where TMethodLogger : IInterceptor
        where TMethodCacheInterceptor : IInterceptor
    {
        public override void RegisterCurrencyRateService(ContainerBuilder builder)
        {
            builder.RegisterType<TCurrencyServer>()
                .As<ICurrencyRateService>()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof(TMethodLogger), typeof(TMethodCacheInterceptor));
        }
    }
}
