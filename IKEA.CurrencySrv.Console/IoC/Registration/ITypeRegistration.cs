﻿using Autofac;

namespace IKEA.CurrencySrv.Console.IoC.Registration
{
    public interface ITypeRegistration
    {
        void Register(ContainerBuilder builder);
    }
}
