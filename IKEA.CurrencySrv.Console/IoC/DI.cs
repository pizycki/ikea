﻿
/* Database Access Layer namespaces */
//#define MONGO
#define EF

/* Rest of #define directives here.
 * They must be at the very top of the file.
 * */


using IKEA.CurrencySrv.Console.Infrastructure;
using IKEA.CurrencySrv.Console.WebServiceModules;

#region [ Database Access Layer namespaces ]

#if MONGO && !EF
using IKEA.Common.Models.DAL.Mongo;
using IKEA.DAL.Mongo;
#endif

#if EF && !MONGO
using IKEA.Common.Models.DAL.EF;
using IKEA.DAL.EF;
#endif

#endregion

using Autofac;
using IKEA.AOP;
using IKEA.Application;
using IKEA.Application.Validators;
using IKEA.Caching;
using IKEA.CurrencyAPI.SOAP.Services;
using IKEA.FCCA.Client;
using IKEA.CurrencySrv.Console.IoC.Registration;
using System.Collections.Generic;

namespace IKEA.CurrencySrv.Console.IoC
{
    public static class DI
    {
        public static IContainer Container
        {
            get { return _container ?? (_container = CreateNewContainerIoc()); }
        }
        private static IContainer _container;

        private static IContainer CreateNewContainerIoc()
        {
            var registrations = new Queue<ITypeRegistration>();
            var builder = new ContainerBuilder();

            /* Mapper */
            registrations.Enqueue(new MapperRegistration<Currency, CurrencyRatio>());

            /* BLL */
            if (Config.UseMemoryCachedCurrencyService)
            {
                registrations.Enqueue(new MemoryCachedCurrencyServerRegistration<
                    CurrencyServer,
                    CurrencyRatioValidatorFactory,
                    CurrencyValidatorFactory,
                    CurrencyServer,
                    MethodLogger,
                    MethodCacheInterceptor>());
            }
            else
            {
                registrations.Enqueue(new CurrencyServerRegistration<
                    CurrencyServer,
                    CurrencyRatioValidatorFactory,
                    CurrencyValidatorFactory,
                    CurrencyServer,
                    MethodLogger>());
            }


            /* Data Providers */
            registrations.Enqueue(new DataProviderRegistration<CurrencyDataProvider>());

            /* Data Access Layer */
            registrations.Enqueue(new DataAccessLayerRegistration<CurrencyRatioDal, CurrencyDal>());

            /* WCF */
            registrations.Enqueue(new WcfRegistration<CurrencyRateService>());

            /* AOP interceptors */
            registrations.Enqueue(new AopInterceptorsRegistration<MethodLogger, MethodCacheInterceptor>());

            /* Cache */
            registrations.Enqueue(new CacheRegistration<Cache, CacheKeyGenerator>());

            /* SOAP */
            registrations.Enqueue(new SoapWsRegistration<SoapWebService>());

            /* REST */
            registrations.Enqueue(new WebApiRegistration<WebApiWebServiceModule>());
            registrations.Enqueue(new NancyRegistration<NancyWebServiceModule>());
            
            // Start registering all services
            RegisterAll(registrations, builder);

            return builder.Build();
        }

        public static void RegisterAll(Queue<ITypeRegistration> registrations, ContainerBuilder builder)
        {
            foreach (var item in registrations)
                item.Register(builder);
        }
    }
}
