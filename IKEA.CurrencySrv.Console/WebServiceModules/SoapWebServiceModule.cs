﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using Autofac.Integration.Wcf;
using IKEA.Common.Contracts.WebServices;
using IKEA.CurrencyAPI.SOAP.Services;
using IKEA.CurrencySrv.Console.IoC;
using NLog;

namespace IKEA.CurrencySrv.Console.WebServiceModules
{
    public class SoapWebService : ISoapWebService
    {
        private ServiceHost _wcfHost;
        private bool _running;

        public string Address { get; private set; }

        public SoapWebService(string address)
        {
            Address = address;
        }

        public void Setup()
        {
            var uri = new Uri(Address); // Adres pod którym będzie można odnaleźć plik WSDL
            _wcfHost = new ServiceHost(typeof(CurrencyRateService), uri);
            _wcfHost.
                AddServiceEndpoint(typeof(CurrencyAPI.SOAP.Contracts.ICurrencyRateService),
                new BasicHttpBinding(), string.Empty); // Określenie sposobu łączenia się z web servicem, HTTP
            _wcfHost.
                AddDependencyInjectionBehavior<CurrencyAPI.SOAP.Contracts.ICurrencyRateService>(DI.Container);

            // Konfiguracja zachowań
            _wcfHost.Description.Behaviors
                .Add(new ServiceMetadataBehavior { HttpGetEnabled = true, HttpGetUrl = uri });

#if DEBUG
            // Włączenie wyświetlania szczegółów wyjątków w wiadomościach zwrotnych
            _wcfHost.Description.Behaviors
                .Remove(typeof(ServiceDebugBehavior));
            _wcfHost.Description.Behaviors
                .Add(new ServiceDebugBehavior { IncludeExceptionDetailInFaults = true });
#endif
        }

        public void Start()
        {
            if (_wcfHost != null)
            {
                _wcfHost.Open();
                _running = true;
            }
        }

        public void Stop()
        {
            _wcfHost.Close();
            _running = false;
        }

        public bool Running
        {
            get { return _running; }
        }
    }
}
