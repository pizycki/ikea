﻿using System.Web.Http;
using Autofac.Integration.WebApi;
using IKEA.Common.Contracts.WebServices;
using IKEA.CurrencySrv.Console.IoC;
using Microsoft.Owin.Hosting;
using Owin;
using NLog;

namespace IKEA.CurrencySrv.Console.WebServiceModules
{
    public class WebApiWebServiceModule : IWebApiWebServiceModule
    {
        public WebApiWebServiceModule(string address)
        {
            Address = address;
            Running = false;
        }

        public void Start()
        {
            WebApp.Start<WebApiStartup>(Address);
            Running = true;
        }

        public void Stop()
        {
            Running = false;
        }

        public void Setup()
        {
            // No setup...
        }

        public bool Running { get; private set; }

        public string Address { get; private set; }
    }

    internal class WebApiStartup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            config.Routes.MapHttpRoute(
                name: "List",
                routeTemplate: "api/{controller}/{action}/{baseAbrv}/{to}/{targetAbrv}",
                defaults: new { to = RouteParameter.Optional, baseAbrv = RouteParameter.Optional, targetAbrv = RouteParameter.Optional }
                );

            var container = DI.Container;

            var resolver = new AutofacWebApiDependencyResolver(container);

            config.DependencyResolver = resolver;

            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
            app.UseWebApi(config);
        }
    }
}
