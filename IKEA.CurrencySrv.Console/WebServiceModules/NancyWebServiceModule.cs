﻿using Autofac;
using IKEA.Common.Contracts.WebServices;
using IKEA.CurrencySrv.Console.IoC;
using Microsoft.Owin.Hosting;
using Nancy.Owin;
using Owin;

namespace IKEA.CurrencySrv.Console.WebServiceModules
{
    public class NancyWebServiceModule : INancyWebServiceModule
    {
        public NancyWebServiceModule(string address)
        {
            Address = address;
            Running = false;
        }

        public void Start()
        {
            WebApp.Start<NancyStartup>(Address);
            Running = true;
        }

        public void Stop()
        {
            Running = false;
        }

        public bool Running { get; private set; }
        public void Setup()
        {
            // No setup..
        }

        public string Address { get; private set; }
    }

    public class NancyBootstrap : Nancy.Bootstrappers.Autofac.AutofacNancyBootstrapper
    {
        protected override ILifetimeScope GetApplicationContainer()
        {
            return DI.Container;
        }
    }

    public class NancyStartup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseNancy(new NancyOptions
            {
                Bootstrapper = new NancyBootstrap()
            });
        }
    }
}
